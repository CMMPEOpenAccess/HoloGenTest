﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoloGen.Alg.Base.Enums
{
    public enum ModulationSchemeType { Phase, Amplitude }
}
