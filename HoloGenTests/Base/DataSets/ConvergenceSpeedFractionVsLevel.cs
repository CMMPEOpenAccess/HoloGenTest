﻿using System.Linq;
using HoloGen.Alg.Base.Enums;
using HoloGen.Options.Algorithm.Termination;

namespace HoloGen.Alg.Base.DataSets
{
    public abstract class ConvergenceSpeedFractionVsLevel : SingleDataSet, ISplitDataSet
    {
        public virtual AlgorithmType Algorithm => AlgorithmType.DS;
        public abstract int NumIterations { get; }
        public abstract ModulationSchemeType ModulationScheme { get; }
        public abstract SeedType Seed { get; }
        public abstract bool IsSplit { get; }

        public virtual float? XMinRight => null;
        public virtual float? XMaxRight => null;
        public virtual float? YMinRight => null;
        public virtual float? YMaxRight => null;
        
        public virtual int[] Levels { get; } = {2, 256};
        public override int NumSubSets => Levels.Length;
        public override float[] XValues => NumPixelsToSwitch.Select(x => (float) x).ToArray();
        public virtual int[] NumPixelsToSwitch { get; } = {1,2,4,8,16,32,64,128,256,512,1024,2048,4096};
        
        private int[] _resolutions;
        public virtual int[] Resolutions => _resolutions ?? (_resolutions = BaseTest.Resolutions(StartPower, MaxPower, DebugTests ? 2 : 10));
        public override string[] LegendNames => Resolutions.Select(x => $"$N={x}$").ToArray();
        public virtual int StartPower => 7;
        public virtual int MaxPower => 12;
        
        public override int NumSeries => DebugTests ? 3 : 50;
        public override LegendLocation LegendLocation => LegendLocation.NorthEast;

        public override string XName => "Iterations";
        public override string YName => "Initial Rate of Convergence";
        public override string XUnits => "Num";
        public override string YUnits => "Num";
        public override string Notes => "Calculated for the set of 512x512 images shown in the thesis/paper. Iterations are spaced by the value of \"Spacing\"";
        public override float? XMin => 0;
        public override float? XMax => NumPixelsToSwitch.Max();
        public override float? YMin => 0;
        public override bool XIsInt => true;
    }
}
