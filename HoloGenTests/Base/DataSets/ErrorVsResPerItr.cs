﻿using System;
using System.Linq;
using HoloGen.Alg.Base.Enums;

namespace HoloGen.Alg.Base.DataSets
{
    public abstract class ErrorVsResPerItr : SingleDataSet, IResolutionBased, ISplitDataSet
    {
        public abstract int NumIterations { get; }
        public abstract ModulationSchemeType ModulationScheme { get; }
        public abstract AlgorithmType Algorithm { get; }
        public abstract SeedType Seed { get; }
        
        public virtual int[] Levels { get; } = {2, 256};
        public abstract int[] IterationCounts { get; }
        public override string[] LegendNames => IterationCounts.Select(x => $"{x} iterations").ToArray();
        public override int NumSubSets => Levels.Length;
        
        public override int NumSeries => DebugTests ? 3 : 10;
        public override LegendLocation LegendLocation => LegendLocation.SouthEast;

        private int[] _resolutions;
        public virtual int[] Resolutions => _resolutions ?? (_resolutions = BaseTest.Resolutions(StartPower, MaxPower, 5));
        public override float[] XValues => Resolutions.Select(x => (float) x).ToArray();
        public virtual int StartPower => 7;
        public virtual int MaxPower => 10;

        public override string XName => "Resolution \\:(pixels)";
        public override string YName => "NMSE";
        public override string XUnits => "pixels";
        public override string YUnits => "Num";
        public override float? XMin => 0;
        public override float? XMax => (float) (Math.Pow(2, MaxPower) * 1.1);
        public override float? YMin => 0;
        
        public virtual float? XMinRight => null;
        public virtual float? XMaxRight => null;
        public virtual float? YMinRight => null;
        public virtual float? YMaxRight => null;
    }
}
