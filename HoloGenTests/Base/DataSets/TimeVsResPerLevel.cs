﻿using System;
using System.Linq;
using HoloGen.Alg.Base.Enums;
using Newtonsoft.Json;

namespace HoloGen.Alg.Base.DataSets
{
    public abstract class TimeVsResPerLevel : SingleDataSet, IResolutionBased
    {
        public abstract int NumIterations { get; }
        [JsonIgnore] public abstract Func<double, double> FitFunction { get; }
        public abstract ModulationSchemeType ModulationScheme { get; }
        public abstract AlgorithmType Algorithm { get; }
        public abstract SeedType Seed { get; }
        
        public virtual int[] Levels { get; } = {2, 3, 4, 8, 16, 32, 64, 128, 256, int.MaxValue};
        //public override string[] LegendNames => Levels.Select(x => x == int.MaxValue ? $"continuous modulation" : $"{x} level modulation").ToArray();
        public override string[] LegendNames => Levels.Select(x => x == int.MaxValue ? $"$\\infty$" : $"{x}").ToArray();
        
        public override int NumSeries => DebugTests ? 3 : 10;
        public override LegendLocation LegendLocation => LegendLocation.NorthWest;
        
        private int[] _resolutions;
        public virtual int[] Resolutions => _resolutions ?? (_resolutions = BaseTest.Resolutions(StartPower, MaxPower, 5));
        public override float[] XValues => Resolutions.Select(x => (float) x).ToArray();
        public virtual int StartPower => 7;
        public virtual int MaxPower => 10;

        public override string XName => "Resolution \\:(pixels)";
        public override string YName => "Iteration \\:Time \\:(ms)";
        public override string XUnits => "pixels";
        public override string YUnits => "ms";
        public override float? XMin => 0;
        public override float? YMin => 0;
    }
}
