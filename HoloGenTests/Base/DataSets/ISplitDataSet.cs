﻿namespace HoloGen.Alg.Base.DataSets
{
    public interface ISplitDataSet
    {
        float? XMinRight { get; }
        float? XMaxRight { get; }
        float? YMinRight { get; }
        float? YMaxRight { get; }
    }
}
