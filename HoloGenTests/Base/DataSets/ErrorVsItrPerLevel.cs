﻿using System.Linq;
using HoloGen.Alg.Base.Enums;
using HoloGen.Options.Algorithm.Termination;

namespace HoloGen.Alg.Base.DataSets
{
    public abstract class ErrorVsItrPerLevel : SingleDataSet, ISplitDataSet
    {
        public abstract int NumIterations { get; }
        public abstract int Spacing { get; }
        public abstract ModulationSchemeType ModulationScheme { get; }
        public abstract AlgorithmType Algorithm { get; }
        public abstract SeedType Seed { get; }
        public abstract bool IsSplit { get; }
        public abstract int SplitSkip { get; }

        public virtual float? XMinRight => null;
        public virtual float? XMaxRight => null;
        public virtual float? YMinRight => null;
        public virtual float? YMaxRight => null;
        
        public override float[] XValues => Enumerable.Range(1, NumIterations).Select(i => (float)i * Spacing).ToArray();
        public virtual int[] Levels { get; } = {2, 3, 4, 8, 16, 32, 64, 128, 256, int.MaxValue};
        //public override string[] LegendNames => Levels.Select(x => x == int.MaxValue ? $"continuous modulation" : $"{x} level modulation").ToArray();
        public override string[] LegendNames => Levels.Select(x => x == int.MaxValue ? $"$\\infty$" : $"{x}").ToArray();
        
        public override int NumSeries => DebugTests ? 3 : 50;
        public override LegendLocation LegendLocation => LegendLocation.NorthEast;

        public override string XName => "Iterations";
        public override string YName => "NMSE";
        public override string XUnits => "Num";
        public override string YUnits => "Num";
        public override string Notes => "Calculated for the set of 512x512 images shown in the thesis/paper. Iterations are spaced by the value of \"Spacing\"";
        public override float? XMin => 0;
        public override float? XMax => NumIterations * Spacing;
        public override float? YMin => 0;
        public override bool XIsInt => true;
    }
}
