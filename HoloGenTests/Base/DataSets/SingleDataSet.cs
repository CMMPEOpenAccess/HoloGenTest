﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace HoloGen.Alg.Base.DataSets
{
    [Serializable]
    public abstract class SingleDataSet : DataSet
    {
        private float[,,,,] _yValues;

        [JsonProperty]
        public virtual float[,,,,] YValues
        {
            get => _yValues ?? (_yValues = new float[NumSubSets, NumLegendEntries, NumImages, NumSeries, NumValues]);
            private set => _yValues = value;
        }

        [JsonIgnore] 
        public virtual FileInfo[] Images => OnlyUseOneImage ? new[]{BaseTest.TestImages[0]} : BaseTest.TestImages;
        
        public virtual int NumSubSets => 1;
        public virtual int NumLegendEntries => LegendNames.Length;
        public int NumImages => OnlyUseOneImage ? 1 : BaseTest.TestImages.Length;
        public abstract int NumSeries { get; }
        public virtual int NumValues => XValues.Length;

        public float this[int legendNo, int imageNo, int seriesNo, int valueNo]
        {
            get => YValues[0, legendNo, imageNo, seriesNo, valueNo];
            set => YValues[0, legendNo, imageNo, seriesNo, valueNo] = value;
        }

        public float this[int subGraphNo, int legendNo, int imageNo, int seriesNo, int valueNo]
        {
            get => YValues[subGraphNo, legendNo, imageNo, seriesNo, valueNo];
            set => YValues[subGraphNo, legendNo, imageNo, seriesNo, valueNo] = value;
        }
    }
}
