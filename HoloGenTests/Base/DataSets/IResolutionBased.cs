﻿namespace HoloGen.Alg.Base.DataSets
{
    public interface IResolutionBased
    {
        int[] Resolutions { get; }
        int StartPower { get; }
        int MaxPower { get; }
    }
}
