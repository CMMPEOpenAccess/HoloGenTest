﻿using System;
using Newtonsoft.Json;

namespace HoloGen.Alg.Base.DataSets
{
    public abstract class MultiDataSet : DataSet
    {
        private SingleDataSet[] _childDataSets;
        public abstract Type[] ChildDataSetTypes { get; }

        [JsonIgnore]
        protected SingleDataSet[] ChildDataSets => _childDataSets ?? (_childDataSets = new SingleDataSet[ChildDataSetTypes.Length]);

        public SingleDataSet this[int childNo]
        {
            get => ChildDataSets[childNo];
            set => ChildDataSets[childNo] = value;
        }
    }
}
