﻿using System.Linq;
using HoloGen.Alg.Base.Enums;

namespace HoloGen.Alg.Base.DataSets
{
    public abstract class ErrorVsLevelCrossSection : CrossSectionDataSet
    {
        public override string XName => "Phase \\:Modulation \\:Levels";
        public override string YName => "NMSE";
        public override string[] LegendNames => new[] {"Randomised Starting Point", "Back Projected Starting Point"};
        public override LegendLocation LegendLocation => LegendLocation.NorthEast;
        public int[] Levels { get; } = {2, 3, 4, 8, 16, 32, 64, 128, 256, int.MaxValue};
        public override float[] XValues => Levels.Select(x => (float) x).ToArray();
        public override bool XIsInt => true;
        public override bool YIsInt => true;
    }
}
