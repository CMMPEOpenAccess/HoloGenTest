﻿using HoloGen.Alg.Base.Enums;

namespace HoloGen.Alg.Base.DataSets.Specific
{
    public abstract class GSErrorVsItrPerLevel : ErrorVsItrPerLevel
    {
        public override int NumIterations => 30;
        public override int Spacing => 1;
        public override bool IsSplit => true;
        public override int SplitSkip => 3;
        
        public override AlgorithmType Algorithm => AlgorithmType.GS;
    }
}
