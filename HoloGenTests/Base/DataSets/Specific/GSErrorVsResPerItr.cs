﻿using HoloGen.Alg.Base.Enums;

namespace HoloGen.Alg.Base.DataSets.Specific
{
    public abstract class GSErrorVsResPerItr : ErrorVsResPerItr
    {
        public override int[] IterationCounts => new[]{2, 5, 10, 20, 30};
        public override int NumIterations => 100;
        public override AlgorithmType Algorithm => AlgorithmType.GS;
    }
}
