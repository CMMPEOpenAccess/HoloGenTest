﻿using System;
using HoloGen.Alg.Base.Enums;
using Newtonsoft.Json;

namespace HoloGen.Alg.Base.DataSets.Specific
{
    public abstract class DSTimeVsResPerLevel : TimeVsResPerLevel
    {
        public override int NumIterations => 1000;
        [JsonIgnore] public override Func<double, double> FitFunction => x => Math.Pow(x * Math.Log(x), 2.0);
        public override AlgorithmType Algorithm => AlgorithmType.DSF;
        public override int NumSeries => DebugTests ? 1 : 50;
    }
}
