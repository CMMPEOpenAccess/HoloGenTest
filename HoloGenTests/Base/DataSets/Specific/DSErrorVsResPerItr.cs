﻿using HoloGen.Alg.Base.Enums;

namespace HoloGen.Alg.Base.DataSets.Specific
{
    public abstract class DSErrorVsResPerItr : ErrorVsResPerItr
    {
        public override int[] IterationCounts => new[]{500, 1000, 5000, 10000, 50000};
        public override int NumIterations => 50000;
        public override AlgorithmType Algorithm => AlgorithmType.DSF;
        public override int NumSeries => DebugTests ? 3 : 50;
    }
}
