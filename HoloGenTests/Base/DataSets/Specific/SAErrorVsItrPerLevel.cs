﻿using HoloGen.Alg.Base.Enums;

namespace HoloGen.Alg.Base.DataSets.Specific
{
    public abstract class SAErrorVsItrPerLevel : ErrorVsItrPerLevel
    {
        public override int NumIterations => 500;
        public override int Spacing => 1000;
        public override bool IsSplit => true;
        public override int SplitSkip => 3;
        public override int NumSeries => DebugTests ? 1 : 50;
        
        public override AlgorithmType Algorithm => AlgorithmType.SAF;
    }
}
