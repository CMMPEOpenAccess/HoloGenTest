﻿using System;
using System.Linq;
using HoloGen.Alg.Base.Enums;
using Newtonsoft.Json;

namespace HoloGen.Alg.Base.DataSets
{
    public abstract class FourierDataSet : FittedDataSet
    {
        public abstract int NumIterations { get; }
        
        public override int NumSeries => DebugTests ? 5 : 50;
        public override LegendLocation LegendLocation => LegendLocation.NorthWest;
        
        public override string XName => "Resolution \\:(pixels)";
        public override string YName => "Iteration \\:Time \\:(ms)";
        public override string XUnits => "Pixels";
        public override string YUnits => "ms";
        public override string Notes => "Calculated for a randomised starting point";
        public override float? XMin => 0;
        public override float? YMin => 0;
        public override float[] XValues => Resolutions.Select(x => (float) x).ToArray();
        public override bool YIsInt => true;
    }
}
