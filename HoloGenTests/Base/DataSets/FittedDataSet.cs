﻿using System;
using System.Linq;
using HoloGen.Alg.Base.Enums;
using Newtonsoft.Json;

namespace HoloGen.Alg.Base.DataSets
{
    public abstract class FittedDataSet : SingleDataSet, IResolutionBased
    {
        [JsonIgnore] public abstract Func<double, double>[] FitFunctions { get; }
        public abstract string[] FitLegendNames { get; }
        
        private int[] _resolutions;
        public virtual int[] Resolutions => _resolutions ?? (_resolutions = BaseTest.Resolutions(StartPower, MaxPower, 5));
        public virtual int StartPower => 8;
        public virtual int MaxPower => 12;
    }
}
