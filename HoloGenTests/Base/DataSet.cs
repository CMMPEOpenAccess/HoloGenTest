﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using HoloGen.Alg.Base.Enums;
using HoloGen.Options.Algorithm.Termination;
using HoloGen.Options.Output.Run;
using Newtonsoft.Json;

namespace HoloGen.Alg.Base
{
    [Serializable]
    public abstract class DataSet
    {
        protected static bool DebugTests = true;
        public static bool OnlyUseOneImage { get; } = true;

        public DateTime DateCreated { get; } 
        
        public abstract string XName { get; }
        public abstract string YName { get; }
        public abstract LegendLocation LegendLocation { get; }
        public abstract string[] LegendNames { get; }
        public abstract float[] XValues { get; }

        public virtual bool Normalise => true;
        public virtual string PrettyName => null;
        public virtual string Title => null;
        public virtual string Notes => null;
        public virtual string Message => null;
        public virtual string XUnits => null;
        public virtual string YUnits => null;
        public virtual float? XMin => null;
        public virtual float? XMax => null;
        public virtual float? YMin => null;
        public virtual float? YMax => null;
        public virtual bool XIsInt => false;
        public virtual bool YIsInt => false;
        
        protected DataSet()
        {
            DateCreated = DateTime.UtcNow;
        }
    }
}
