﻿// Copyright 2018 (C) Peter J. Christopher - All Rights Reserved.
// 
// Unauthorized copying of this application, via any medium is strictly prohibited.
// Proprietary and confidential.
// 
// Please contact Peter J. Christopher in the first instance for inquiries of any kind. 
// Failing this, The Centre of Molecular Materials for Photonics and Electronics (CMMPE) 
// at the University of Cambridge may also be able to assist.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using HoloGen.Alg.Base.DataSets;
using HoloGen.Alg.Base.DataSets.Specific;
using HoloGen.Alg.Tests;
using HoloGen.Alg.Tests.DS;
using HoloGen.Alg.Tests.GS;
using HoloGen.Alg.Tests.SA;
using HoloGen.Image;
using HoloGen.IO;
using HoloGen.Utils;
using Newtonsoft.Json;

namespace HoloGen.Alg.Base
{
    class BenchmarkingTests
    {
        public Scale Scale = new Scale(0, 3);
        
        private static readonly FileInfo DocumentPath = new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Thesis\\oneChapter.tex");

        private readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings
        { 
            TypeNameHandling = TypeNameHandling.Objects,
            Formatting = Formatting.Indented
        };

        public List<SingleDataSet> SingleDataSets = new List<SingleDataSet>();
        public List<MultiDataSet> MultiDataSets = new List<MultiDataSet>();
        public List<DataSet> DataSets => new List<DataSet>().Concat(SingleDataSets).Concat(MultiDataSets).ToList();
        
        public List<Type> BlackList = new List<Type>
        {
            typeof(GSAmpBackProjectErrorVsItrPerLevel),
            typeof(GSAmpErrorVsLevelCrossSection),
            typeof(GSAmpErrorVsResPerItr),
            typeof(GSAmpItrVsLevelCrossSection),
            typeof(GSAmpRandomErrorVsItrPerLevel),
            typeof(GSAmpTimeVsResPerLevel),
            typeof(DSAmpBackProjectErrorVsItrPerLevel),
            typeof(DSAmpErrorVsLevelCrossSection),
            typeof(DSAmpErrorVsResPerItr),
            typeof(DSAmpItrVsLevelCrossSection),
            typeof(DSAmpRandomErrorVsItrPerLevel),
            typeof(DSAmpTimeVsResPerLevel),
            typeof(SAAmpBackProjectErrorVsItrPerLevel),
            typeof(SAAmpErrorVsLevelCrossSection),
            typeof(SAAmpErrorVsResPerItr),
            typeof(SAAmpItrVsLevelCrossSection),
            typeof(SAAmpRandomErrorVsItrPerLevel),
            typeof(SAAmpTimeVsResPerLevel),
        };
        public List<Type> NeedsRun = new List<Type>
        {
            typeof(DSPhaseBackProjectErrorVsItrPerLevel),
            typeof(DSPhaseRandomErrorVsItrPerLevel),
            typeof(SAPhaseBackProjectErrorVsItrPerLevel),
            typeof(SAPhaseRandomErrorVsItrPerLevel),
            typeof(DSPhaseErrorVsResPerItr),
            typeof(SAPhaseErrorVsResPerItr),
            //typeof(DSPhaseErrorVsLevelCrossSection),
            //typeof(DSPhaseItrVsLevelCrossSection),
            //typeof(DSPhaseTimeVsResPerLevel),
            //typeof(SAPhaseErrorVsLevelCrossSection),
            //typeof(SAPhaseItrVsLevelCrossSection),
            //typeof(SAPhaseTimeVsResPerLevel),
            //typeof(GSPhaseBackProjectErrorVsItrPerLevel),
            //typeof(GSPhaseErrorVsLevelCrossSection),
            //typeof(GSPhaseErrorVsResPerItr),
            //typeof(GSPhaseItrVsLevelCrossSection),
            //typeof(GSPhaseRandomErrorVsItrPerLevel),
            //typeof(GSPhaseTimeVsResPerLevel),
            //typeof(DSPhaseConvergenceSpeed),
        };
        public List<Type> NeedsGraph = new List<Type>
        {
            //typeof(GSPhaseRandomErrorVsItrPerLevel),
            //typeof(GSPhaseBackProjectErrorVsItrPerLevel),
            //typeof(DSPhaseRandomErrorVsItrPerLevel),
            //typeof(DSPhaseBackProjectErrorVsItrPerLevel),
            //typeof(SAPhaseRandomErrorVsItrPerLevel),
            //typeof(SAPhaseBackProjectErrorVsItrPerLevel),
        };

        public bool RunAll = false;
        public bool GraphAll = false;
        public bool SkipRun = false;
        public bool SkipParent = true;

        public void Run()
        {
            Log("Loading Existing Single Data Sets...");
            var singleDataSetTypes = typeof(SingleDataSet).Assembly.GetTypes()
                .Where(type => type.IsSubclassOf(typeof(SingleDataSet)) && 
                               !type.IsAbstract &&
                               !BlackList.Contains(type));
            foreach (var dataSetType in singleDataSetTypes)
            {
                if (RunAll) 
                    NeedsRun.Add(dataSetType);
                if (GraphAll) 
                    NeedsGraph.Add(dataSetType);

                if(!NeedsRun.Contains(dataSetType) || SkipRun)
                {
                    var dataFileName = $"{BaseTest.ResultPath.FullName}\\{dataSetType.Name}.json";
                    if (new FileInfo(dataFileName).Exists)
                    {
                        var dataFileContents = File.ReadAllText(dataFileName);
                        var dataSetFromFile = (SingleDataSet) JsonConvert.DeserializeObject(dataFileContents, dataSetType);
                        if (dataSetFromFile != null)
                        {
                            SingleDataSets.Add(dataSetFromFile);
                            continue;
                        }
                    }
                }

                var singleDataSet = (SingleDataSet)Activator.CreateInstance(dataSetType, true);
                SingleDataSets.Add(singleDataSet);
                if (!NeedsRun.Contains(singleDataSet.GetType()) && ! SkipRun) 
                    NeedsRun.Add(singleDataSet.GetType());
                if (!NeedsGraph.Contains(singleDataSet.GetType()) && ! SkipRun) 
                    NeedsGraph.Add(singleDataSet.GetType());
            }

            Log("Creating Multi Data Set Links...");
            var multiDataSetTypes = typeof(MultiDataSet).Assembly.GetTypes()
                .Where(type => type.IsSubclassOf(typeof(MultiDataSet)) && 
                               !type.IsAbstract &&
                               !BlackList.Contains(type));
            foreach (var dataSetType in multiDataSetTypes)
            {
                var multiDataSet = (MultiDataSet)Activator.CreateInstance(dataSetType, true);
                MultiDataSets.Add(multiDataSet);
                if (GraphAll) 
                    NeedsGraph.Add(dataSetType);
                for (var childIndex = 0; childIndex < multiDataSet.ChildDataSetTypes.Length; childIndex++)
                {
                    multiDataSet[childIndex] = SingleDataSets.Single(x => x.GetType() == multiDataSet.ChildDataSetTypes[childIndex]);
                    if (!NeedsRun.Contains(dataSetType)) continue;
                    if (!NeedsRun.Contains(multiDataSet.ChildDataSetTypes[childIndex])) 
                        NeedsRun.Add(multiDataSet.ChildDataSetTypes[childIndex]);
                    if (!NeedsGraph.Contains(multiDataSet.ChildDataSetTypes[childIndex])) 
                        NeedsGraph.Add(multiDataSet.ChildDataSetTypes[childIndex]);
                }
            }

            Log("Flagging Missing Graphs...");
            foreach (var dataSet in MultiDataSets)
            {
                var graphFileName = $"{BaseTest.GraphPath.FullName}\\{dataSet.GetType().Name}.tex";
                if (!new FileInfo(graphFileName).Exists)
                    if (!NeedsGraph.Contains(dataSet.GetType())) 
                        NeedsGraph.Add(dataSet.GetType());
            }
            
            Log("Converting images...");
            var pngFiles = Directory.GetFiles(BaseTest.InputPath.FullName, "*.png");
            int convNum = 0;
            var resolutions = BaseTest.Resolutions(7, 10);
            foreach(var pngFile in pngFiles)
            {
                Log($"Converting image {++convNum} of {pngFiles.Length * (resolutions.Length + 1)}...");
                ConvertPNGtoHFIMG(Path.GetFileNameWithoutExtension(pngFile));
                foreach(var resolution in resolutions)
                {
                    Log($"Converting image {++convNum} of {pngFiles.Length * (resolutions.Length + 1)}...");
                    ConvertPNGtoHFIMG(Path.GetFileNameWithoutExtension(pngFile), resolution);
                }
            }
            Log(" ");

            Log("Running Single Tests...");
            if (!SkipRun)
            {
                var testNum = 0;
                foreach (var dataSet in DataSets)
                {
                    if (!NeedsRun.Contains(dataSet.GetType())) continue;
                    Log($"Running test {++testNum} of {NeedsRun.Count}...");
                    BaseTest.RunTest(dataSet, Log);
                    var fileContents = JsonConvert.SerializeObject(dataSet, _jsonSerializerSettings);
                    var fileName = $"{BaseTest.ResultPath.FullName}\\{dataSet.GetType().Name}.json";
                    File.WriteAllText(fileName, fileContents);
                }
            }

            Log("Making Graphs...");
            var makeNum = 0;
            var graphDataSets = DataSets.Where(x => NeedsGraph.Contains(x.GetType())).ToList();
            foreach (var graphDataSet in graphDataSets)
            { 
                Log($"Making graph {++makeNum} of {graphDataSets.Count}...");
                var graphString = GraphMaker.MakeGraph(graphDataSet).ToString();
                File.WriteAllText($"{BaseTest.GraphPath.FullName}\\{graphDataSet.GetType().Name}.tex", graphString);
            }
            
            Log("Making pdfs...");
            var pdfNum = 0;
            foreach (var graphDataSet in graphDataSets)
            {
                Log($"Making pdf {++pdfNum} of {graphDataSets.Count}...");
                var cmd = new Process();
                cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.StartInfo.WorkingDirectory = BaseTest.GraphPath.FullName;
                cmd.Start();
                cmd.StandardInput.WriteLine($"pdflatex.exe -synctex=1 -shell-escape -enable-write18 -quiet -interaction=nonstopmode  \"{BaseTest.GraphPath.FullName}\\{graphDataSet.GetType().Name}.tex\"");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                Console.WriteLine();
            }
            
            Log("Making parent pdf...");
            if (!SkipParent) {
                var cmd = new Process();
                cmd.StartInfo.FileName = "cmd.exe";
                cmd.StartInfo.RedirectStandardInput = true;
                cmd.StartInfo.UseShellExecute = false;
                cmd.StartInfo.WorkingDirectory = DocumentPath.Directory?.FullName ?? "";
                cmd.Start();
                cmd.StandardInput.WriteLine($"pdflatex.exe -synctex=1 -shell-escape -enable-write18 -quiet  -interaction=nonstopmode  \"{DocumentPath}\"");
                cmd.StandardInput.Flush();
                cmd.StandardInput.Close();
                cmd.WaitForExit();
                Console.WriteLine();
            }

            Log("Cleaning up...");
        }

        private static void Log(string message)
        {
            if (message.Length >= 0)
            {
                string timedMessage = Time.Timestamp() + message;
                Console.WriteLine(timedMessage);
            }
            else
            {
                Console.WriteLine(message);
            }
        }
        
        
        private static void ConvertPNGtoHFIMG(string name, int resolution = 0)
        {
            var outputFile = new FileInfo(BaseTest.InputPath.FullName + "\\" + (resolution > 0 ? "Scaled\\" : "") + name + (resolution > 0 ? "_" + resolution : "") + ".hfimg");
            if (outputFile.Exists) return;

            var image = System.Drawing.Image.FromFile(BaseTest.InputPath.FullName + "\\" + name + ".png");
            var bitmap = resolution > 0 ? ResizeImage(image, resolution, resolution) : new Bitmap(image);

            var size = bitmap.Size;

            var bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, size.Width, size.Height),
                ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb
            );
            var bitmapBytes = new byte[Math.Abs(bitmapData.Stride) * bitmapData.Height];
            Marshal.Copy(bitmapData.Scan0, bitmapBytes, 0, Math.Abs(bitmapData.Stride) * bitmapData.Height);

            var values = new Complex[size.Height, size.Width];

            long location = 0;
            for(var y = 0; y < size.Height; y++)
            {
                for(var x = 0; x < size.Width; x++)
                {
                    // Values from https://en.wikipedia.org/wiki/Grayscale
                    // WARNING: GDI+ Uses the order blue, green, red!
                    var norm = ((bitmapBytes[location + 2] * 30) + (bitmapBytes[location + 1] * 59) + (bitmapBytes[location] * 11)) / 100.0;
                    values[y, x] = new Complex(norm, 0);

                    location += 3;
                }
            }

            bitmap.UnlockBits(bitmapData);
            var complexImage = new ComplexImage(values);
            if (outputFile.Directory != null && !outputFile.Directory.Exists)
            {
                Log($"Directory does not exist. Ignoring.");
            }
            if (outputFile.Exists)
            {
                new JsonImageFileSaver().Save(outputFile, complexImage);
                Log($"PNG file {name}.png converted to {name}.hfimg. We overwrote it because that's what naughty programs do.");
            }
            else
            {
                new JsonImageFileSaver().Save(outputFile, complexImage);
            }
        }

        // Modified from:
        // https://stackoverflow.com/questions/1922040/how-to-resize-an-image-c-sharp
        public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width,image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
    }
}
