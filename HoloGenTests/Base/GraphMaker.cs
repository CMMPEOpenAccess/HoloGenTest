﻿// Copyright 2018 (C) Peter J. Christopher - All Rights Reserved.
// 
// Unauthorized copying of this application, via any medium is strictly prohibited.
// Proprietary and confidential.
// 
// Please contact Peter J. Christopher in the first instance for inquiries of any kind. 
// Failing this, The Centre of Molecular Materials for Photonics and Electronics (CMMPE) 
// at the University of Cambridge may also be able to assist.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using HoloGen.Alg.Base.DataSets;
using HoloGen.Alg.Base.Enums;
using HoloGen.Options.Algorithm.Termination;
using HoloGen.Options.Output.Run;
using HoloGen.Utils.ExtensionMethods;
using MathNet.Numerics;

namespace HoloGen.Alg.Base
{
    public enum AxisSize { Full, Left, Right }

    public static class GraphMaker
    {
        private const float FullWidth = 14.75f;
        private const float LeftWidth = 7.4f;
        private const float RightWidth = 7.4f;
        private const float FullHeight = 6.5f;

        public static StringBuilder MakeGraph(DataSet dataSet)
        {
            StringBuilder builder = new StringBuilder();

            MakeDocument(0, builder, dataSet);

            return builder;
        }

        private static void MakeDocument(int indent, StringBuilder builder, DataSet dataSet)
        {
            builder.AppendLine(Indent(indent) + $"%!TEX root = {dataSet.GetType().Name}.tex");
            builder.AppendLine(Indent(indent) + $"\\documentclass[tikz]{{standalone}}");
            builder.AppendLine(Indent(indent) + $"\\usepackage{{pgfplots}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{uniRed}}{{RGB}}{{171,31,45}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{uniStone}}{{RGB}}{{190,185,166}}");
            builder.AppendLine(Indent(indent) + $"%\\definecolor{{graphColor1}}{{HTML}}{{806ab7}}");
            builder.AppendLine(Indent(indent) + $"%\\definecolor{{graphColor2}}{{HTML}}{{1c7698}}");
            builder.AppendLine(Indent(indent) + $"%\\definecolor{{graphColor3}}{{HTML}}{{06827d}}");
            builder.AppendLine(Indent(indent) + $"%\\definecolor{{graphColor4}}{{HTML}}{{2c882e}}");
            builder.AppendLine(Indent(indent) + $"%\\definecolor{{graphColor5}}{{HTML}}{{7e8b27}}");
            builder.AppendLine(Indent(indent) + $"%\\definecolor{{graphColor6}}{{HTML}}{{c28a0e}}");
            builder.AppendLine(Indent(indent) + $"%\\definecolor{{graphColor7}}{{HTML}}{{c9002f}}");
            builder.AppendLine(Indent(indent) + $"%\\definecolor{{graphColor8}}{{HTML}}{{cb238e}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{graphColor1}}{{HTML}}{{42145f}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{graphColor2}}{{HTML}}{{002f5f}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{graphColor3}}{{HTML}}{{003d4c}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{graphColor4}}{{HTML}}{{024731}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{graphColor5}}{{HTML}}{{53682b}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{graphColor6}}{{HTML}}{{86431e}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{graphColor7}}{{HTML}}{{5e3032}}");
            builder.AppendLine(Indent(indent) + $"\\definecolor{{graphColor8}}{{HTML}}{{772059}}");
            builder.AppendLine(Indent(indent) + $"\\pgfplotsset{{compat=newest}}");
            builder.AppendLine(Indent(indent) + $"\\usetikzlibrary{{pgfplots.colorbrewer}}");
            builder.AppendLine(Indent(indent) + $"\\usetikzlibrary{{plotmarks}}");
            builder.AppendLine(Indent(indent) + $"\\usetikzlibrary{{spy}}");
            builder.AppendLine(Indent(indent) + $"\\usepgfplotslibrary{{groupplots}}");
            builder.AppendLine(Indent(indent) + $"\\usepgfplotslibrary{{external}}");
            builder.AppendLine(Indent(indent) + $"\\usepgfplotslibrary{{patchplots}}");
            builder.AppendLine(Indent(indent) + $"\\pgfplotsset{{");
            builder.AppendLine(Indent(indent) + $"    SimpleSeries/.style={{");
            builder.AppendLine(Indent(indent) + $"        cycle list={{");
            builder.AppendLine(Indent(indent) + $"                {{uniRed,solid,no markers}},");
            builder.AppendLine(Indent(indent) + $"                {{graphColor1,solid,no markers}},");
            builder.AppendLine(Indent(indent) + $"                {{graphColor2,solid,no markers}},");
            builder.AppendLine(Indent(indent) + $"                {{graphColor3,solid,no markers}},");
            builder.AppendLine(Indent(indent) + $"                {{graphColor4,solid,no markers}},");
            builder.AppendLine(Indent(indent) + $"                {{graphColor5,solid,no markers}},");
            builder.AppendLine(Indent(indent) + $"                {{graphColor6,solid,no markers}},");
            builder.AppendLine(Indent(indent) + $"                {{graphColor7,solid,no markers}},");
            builder.AppendLine(Indent(indent) + $"                {{graphColor8,solid,no markers}},");
            builder.AppendLine(Indent(indent) + $"                %{{uniRed,solid,mark=+}},");
            builder.AppendLine(Indent(indent) + $"                %{{uniRed,densely dotted,mark=x}},");
            builder.AppendLine(Indent(indent) + $"                %{{uniRed,loosely dotted,mark=Mercedes star}},");
            builder.AppendLine(Indent(indent) + $"                %{{uniRed,densely dashed,mark=Mercedes star flipped}},");
            builder.AppendLine(Indent(indent) + $"                %{{uniStone,solid,mark=+}},");
            builder.AppendLine(Indent(indent) + $"                %{{uniStone,densely dotted,mark=x}},");
            builder.AppendLine(Indent(indent) + $"                %{{uniStone,loosely dotted,mark=Mercedes star}},");
            builder.AppendLine(Indent(indent) + $"                %{{uniStone,densely dashed,mark=Mercedes star flipped}},");
            builder.AppendLine(Indent(indent) + $"            }},");
            builder.AppendLine(Indent(indent) + $"            legend style={{");
            builder.AppendLine(Indent(indent) + $"            at={{(0.5,-0.2)}},");
            builder.AppendLine(Indent(indent) + $"            anchor=north,");
            builder.AppendLine(Indent(indent) + $"            legend columns=2,");
            builder.AppendLine(Indent(indent) + $"                cells={{anchor=west}},");
            builder.AppendLine(Indent(indent) + $"                font=\\footnotesize,");
            builder.AppendLine(Indent(indent) + $"            rounded corners=2pt,");
            builder.AppendLine(Indent(indent) + $"        }},");
            builder.AppendLine(Indent(indent) + $"        height=6.8cm,");
            builder.AppendLine(Indent(indent) + $"        width=14.75cm,");
            builder.AppendLine(Indent(indent) + $"        every axis/.append style={{");
            builder.AppendLine(Indent(indent) + $"            line width=1.0pt,");
            builder.AppendLine(Indent(indent) + $"            label style={{font=\\footnotesize}},");
            builder.AppendLine(Indent(indent) + $"            tick label style={{font=\\footnotesize}}");
            builder.AppendLine(Indent(indent) + $"        }},");
            builder.AppendLine(Indent(indent) + $"    }},");
            builder.AppendLine(Indent(indent) + $"    colormap={{redgrey}}{{");
            builder.AppendLine(Indent(indent) + $"        rgb255=(190,185,166)");
            builder.AppendLine(Indent(indent) + $"        rgb255=(171,31,45)");
            builder.AppendLine(Indent(indent) + $"    }},");
            builder.AppendLine(Indent(indent) + $"    colormap={{rainbow}}{{");
            builder.AppendLine(Indent(indent) + $"        HTML=(42145f)");
            builder.AppendLine(Indent(indent) + $"        HTML=(002f5f)");
            builder.AppendLine(Indent(indent) + $"        HTML=(003d4c)");
            builder.AppendLine(Indent(indent) + $"        HTML=(024731)");
            builder.AppendLine(Indent(indent) + $"        HTML=(53682b)");
            builder.AppendLine(Indent(indent) + $"        HTML=(86431e)");
            builder.AppendLine(Indent(indent) + $"        HTML=(5e3032)");
            builder.AppendLine(Indent(indent) + $"        HTML=(772059)");
            builder.AppendLine(Indent(indent) + $"    }},");
            builder.AppendLine(Indent(indent) + $"}}");
            builder.AppendLine(Indent(indent) + $"\\begin{{document}}");
            builder.AppendLine(Indent(indent) + $"    \\begin{{tikzpicture}}");
            switch (dataSet)
            {
                case ErrorVsItrPerLevel convergenceDataSet:
                    MakeDataSet(indent + 2, builder, convergenceDataSet);
                    break;
                case FourierDataSet fourierDataSet:
                    MakeDataSet(indent + 2, builder, fourierDataSet);
                    break;
                case ErrorVsLevelCrossSection levelConvergenceComparisonDataSet:
                    MakeDataSet(indent + 2, builder, levelConvergenceComparisonDataSet);
                    break;
                case ItrVsLevelCrossSection itrConvergenceComparisonDataSet:
                    MakeDataSet(indent + 2, builder, itrConvergenceComparisonDataSet);
                    break;
                case TimeVsResPerLevel resolutionTimeComparisonDataSet:
                    MakeDataSet(indent + 2, builder, resolutionTimeComparisonDataSet);
                    break;
                case ErrorVsResPerItr resolutionErrorComparisonDataSet:
                    MakeDataSet(indent + 2, builder, resolutionErrorComparisonDataSet);
                    break;
                case ConvergenceSpeedFractionVsLevel convergenceSpeedFractionVsLevel:
                    MakeDataSet(indent + 2, builder, convergenceSpeedFractionVsLevel);
                    break;
                default:
                    throw new NotImplementedException("Unknown Data Set Type");
            }
            builder.AppendLine(Indent(indent) + $"    \\end{{tikzpicture}}");
            builder.AppendLine(Indent(indent) + $"\\end{{document}}");
        }

        private static void MakeDataSet(int indent, StringBuilder builder, ErrorVsItrPerLevel dataSet)
        {
            builder.AppendLine(Indent(indent) + $"\\begin{{axis}}[");
            AddSimpleAxis(indent + 1, builder, dataSet);
            AddAxisSize(indent + 1, builder, dataSet, dataSet.IsSplit ? AxisSize.Left : AxisSize.Full);
            AddAxisNames(indent + 1, builder, dataSet);
            AddAxisLimits(indent + 1, builder, dataSet);
            AddLegend(indent + 1, builder, dataSet.LegendLocation);
            builder.AppendLine(Indent(indent) + $"    ]");
            AddPlotElements(indent + 1, builder, dataSet, true, 0, 0);
            builder.AppendLine(Indent(indent) + $"\\end{{axis}}");

            if (!dataSet.IsSplit) return;
            
            builder.AppendLine(Indent(indent) + $"\\begin{{axis}}[");
            Add3DAxis(indent + 1, builder, dataSet);
            AddAxisSize(indent + 1, builder, dataSet, AxisSize.Right);
            Add3DAxisNames(indent + 1, builder, dataSet, "Levels");
            Add3DAxisLimits(indent + 1, builder, dataSet);
            builder.AppendLine(Indent(indent) + $"    ]");
            Add3DPlotElements(indent + 1, builder, dataSet, "x", dataSet.XMin ?? 0);
            Add3DPlotElements(indent + 1, builder, dataSet, "y"); // todo: Test here
            Add3DPlotElements(indent + 1, builder, dataSet, "z", dataSet.YMin ?? 0);
            Add3DPlotElements(indent + 1, builder, dataSet);
            builder.AppendLine(Indent(indent) + $"\\end{{axis}}");
        }
    
        private static void MakeDataSet(int indent, StringBuilder builder, FourierDataSet dataSet)
        {
            builder.AppendLine(Indent(indent) + $"\\begin{{axis}}[");
            AddXLogAxis(indent + 1, builder, dataSet);
            AddAxisSize(indent + 1, builder, dataSet, AxisSize.Full);
            AddAxisNames(indent + 1, builder, dataSet);
            AddAxisLimits(indent + 1, builder, dataSet);
            AddLegend(indent + 1, builder, dataSet.LegendLocation);
            builder.AppendLine(Indent(indent) + $"    ]");
            AddPlotElements(indent + 1, builder, dataSet, true, 0, 0);
            AddFitElements(indent + 1, builder, dataSet, true, 0, 0);
            builder.AppendLine(Indent(indent) + $"\\end{{axis}}");
        }
    
        private static void MakeDataSet(int indent, StringBuilder builder, ErrorVsLevelCrossSection dataSet)
        {
            builder.AppendLine(Indent(indent) + $"\\begin{{axis}}[");
            AddLogLogAxis(indent + 1, builder, dataSet);
            AddAxisSize(indent + 1, builder, dataSet);
            AddAxisNames(indent + 1, builder, dataSet);
            AddAxisLimits(indent + 1, builder, dataSet);
            AddLegend(indent + 1, builder, dataSet.LegendLocation);
            builder.AppendLine(Indent(indent) + $"    ]");
            AddCrossSectionPlotElements(indent + 1, builder, dataSet, true, true, dataSet[0].XValues.Length-1, 0);
            builder.AppendLine(Indent(indent) + $"\\end{{axis}}");
        }
    
        private static void MakeDataSet(int indent, StringBuilder builder, ItrVsLevelCrossSection dataSet)
        {
            builder.AppendLine(Indent(indent) + $"\\begin{{axis}}[");
            AddXLogAxis(indent + 1, builder, dataSet);
            AddAxisSize(indent + 1, builder, dataSet);
            AddAxisNames(indent + 1, builder, dataSet);
            AddAxisLimits(indent + 1, builder, dataSet);
            AddLegend(indent + 1, builder, dataSet.LegendLocation);
            builder.AppendLine(Indent(indent) + $"    ]");
            AddConvergencePlotElements(indent + 1, builder, dataSet, true, true, 0);
            builder.AppendLine(Indent(indent) + $"\\end{{axis}}");
        }
    
        private static void MakeDataSet(int indent, StringBuilder builder, TimeVsResPerLevel dataSet)
        {
            builder.AppendLine(Indent(indent) + $"\\begin{{axis}}[");
            AddXLogAxis(indent + 1, builder, dataSet);
            AddAxisSize(indent + 1, builder, dataSet);
            AddAxisNames(indent + 1, builder, dataSet);
            AddAxisLimits(indent + 1, builder, dataSet);
            AddLegend(indent + 1, builder, dataSet.LegendLocation);
            builder.AppendLine(Indent(indent) + $"    ]");
            AddPlotElements(indent + 1, builder, dataSet, true, 0, 0);
            builder.AppendLine(Indent(indent) + $"\\end{{axis}}");
        }
    
        private static void MakeDataSet(int indent, StringBuilder builder, ErrorVsResPerItr dataSet)
        {

            builder.AppendLine(Indent(indent) + $"\\begin{{axis}}[");
            AddXLogAxis(indent + 1, builder, dataSet);
            AddAxisSize(indent + 1, builder, dataSet, AxisSize.Left);
            AddAxisNames(indent + 1, builder, dataSet);
            AddTiedAxisLimits(indent + 1, builder, dataSet);
            AddLegend(indent + 1, builder, dataSet.LegendLocation);
            builder.AppendLine(Indent(indent) + $"    ]");
            AddPlotElements(indent + 1, builder, dataSet, true, 0, 0);
            builder.AppendLine(Indent(indent) + $"\\end{{axis}}");
            AddMessage(builder, dataSet.Levels[0] == 2 ? "binary modulation" : $"{dataSet.Levels[0]} level modulation", true);
            
            builder.AppendLine(Indent(indent) + $"\\begin{{axis}}[");
            AddXLogAxis(indent + 1, builder, dataSet);
            AddAxisSize(indent + 1, builder, dataSet, AxisSize.Right);
            AddAxisNames(indent + 1, builder, dataSet);
            AddTiedAxisLimits(indent + 1, builder, dataSet);
            AddLegend(indent + 1, builder, dataSet.LegendLocation);
            builder.AppendLine(Indent(indent) + $"    ]");
            AddPlotElements(indent + 1, builder, dataSet, true, 1, 0);
            builder.AppendLine(Indent(indent) + $"\\end{{axis}}");
            AddMessage(builder, dataSet.Levels[1] == 2 ? "binary modulation" : $"{dataSet.Levels[1]} level modulation", false);
        }
        
        private static void MakeDataSet(int indent, StringBuilder builder, ConvergenceSpeedFractionVsLevel dataSet)
        {
            builder.AppendLine(Indent(indent) + $"\\begin{{axis}}[");
            Add3DAxis(indent + 1, builder, dataSet);
            AddAxisSize(indent + 1, builder, dataSet, AxisSize.Left);
            Add3DAxisNames(indent + 1, builder, dataSet, "Levels");
            Add3DAxisLimits(indent + 1, builder, dataSet);
            builder.AppendLine(Indent(indent) + $"    ]");
            Add3DPlotElements(indent + 1, builder, dataSet, "x", dataSet.XMin ?? 0);
            Add3DPlotElements(indent + 1, builder, dataSet, "y"); // todo: Test here
            Add3DPlotElements(indent + 1, builder, dataSet, "z", dataSet.YMin ?? 0);
            Add3DPlotElements(indent + 1, builder, dataSet);
            builder.AppendLine(Indent(indent) + $"\\end{{axis}}");
        }

        private static void AddTiedAxisLimits(int indent, StringBuilder builder, ErrorVsResPerItr dataSet)
        {
            var normalisationFactors = dataSet.Normalise ? CalculateNormalisationFactors(dataSet) : Enumerable.Repeat(1.0f, dataSet.NumImages).ToArray();
            var means = CalculateImageMeans(dataSet, normalisationFactors);
            var eMeans = from float item in means
                select item;
            var yMax = eMeans.Max();
            var yMin = eMeans.Min();
            yMax = dataSet.YMax != null ? dataSet.YMax ?? yMax : yMax;
            yMin = dataSet.YMin != null ? dataSet.YMin ?? yMin : yMin;
            var gap = yMax - yMin;
            if(dataSet.YMax == null)
                yMax += 0.12f * gap;
            if(dataSet.YMin == null)
                yMin -= 0.12f * gap;

            if(dataSet.XMax != null)
                builder.AppendLine(Indent(indent) + $"xmax={dataSet.XMax},");
            if(dataSet.XMin != null)
                builder.AppendLine(Indent(indent) + $"xmin={dataSet.XMin},");
            builder.AppendLine(Indent(indent) + $"ymax={yMax},");
            builder.AppendLine(Indent(indent) + $"ymin={yMin},");
        }
        
        private static void AddAxisSize(int indent, StringBuilder builder, DataSet dataSet, AxisSize size = AxisSize.Full)
        {
            builder.AppendLine(Indent(indent) + $"width={(size == AxisSize.Full ? FullWidth : size == AxisSize.Left ? LeftWidth : RightWidth)}cm,");
            builder.AppendLine(Indent(indent) + $"height={FullHeight}cm,");
            if (size == AxisSize.Right)
                builder.AppendLine(Indent(indent) + $"xshift={FullWidth - RightWidth}cm,");
        }
        
        private static void AddAxisNames(int indent, StringBuilder builder, DataSet dataSet)
        {
            if(dataSet.XName != null)
                builder.AppendLine(Indent(indent) + $"xlabel=${dataSet.XName}$,");
            if(dataSet.YName != null)
                builder.AppendLine(Indent(indent) + $"ylabel=${dataSet.YName}$,");
        }
        
        private static void Add3DAxisNames(int indent, StringBuilder builder, DataSet dataSet, string zName)
        {
            if(dataSet.XName != null)
                builder.AppendLine(Indent(indent) + $"xlabel=${dataSet.XName}$,");
            if(dataSet.YName != null)
                builder.AppendLine(Indent(indent) + $"zlabel=${dataSet.YName}$,");
            if(zName != null)
                builder.AppendLine(Indent(indent) + $"ylabel=${zName}$,");
        }

        private static void Add3DAxisLimits(int indent, StringBuilder builder, DataSet dataSet, float? zMax = null, float? zMin = null)
        {
            if(dataSet.XMax != null) 
                builder.AppendLine(Indent(indent) + $"xmax={dataSet.XMax},");
            if(dataSet.XMin != null) 
                builder.AppendLine(Indent(indent) + $"xmin={dataSet.XMin},");
            if(dataSet.YMax != null) 
                builder.AppendLine(Indent(indent) + $"zmax={dataSet.YMax},");
            if(dataSet.YMin != null) 
                builder.AppendLine(Indent(indent) + $"zmin={dataSet.YMin},");
            if(zMax != null) 
                builder.AppendLine(Indent(indent) + $"ymax={zMax},");
            if(zMin != null) 
                builder.AppendLine(Indent(indent) + $"ymin={zMin},");
        }

        private static void AddAxisLimits(int indent, StringBuilder builder, DataSet dataSet)
        {
            if(dataSet.XMax != null) 
                builder.AppendLine(Indent(indent) + $"xmax={dataSet.XMax},");
            if(dataSet.XMin != null) 
                builder.AppendLine(Indent(indent) + $"xmin={dataSet.XMin},");
            if(dataSet.YMax != null) 
                builder.AppendLine(Indent(indent) + $"ymax={dataSet.YMax},");
            if(dataSet.YMin != null) 
                builder.AppendLine(Indent(indent) + $"ymin={dataSet.YMin},");
        }

        private static void AddAxisLimitsRight(int indent, StringBuilder builder, SingleDataSet dataSet)
        {
            if(dataSet.XMax != null) 
                builder.AppendLine(Indent(indent) + $"xmax={dataSet.XMax},");
            if(dataSet.XMin != null) 
                builder.AppendLine(Indent(indent) + $"xmin={dataSet.XMin},");
            if(dataSet.YMax != null) 
                builder.AppendLine(Indent(indent) + $"ymax={dataSet.YMax},");
            if(dataSet.YMin != null) 
                builder.AppendLine(Indent(indent) + $"ymin={dataSet.YMin},");
        }
        
        private static void AddLogLogAxis(int indent, StringBuilder builder, DataSet dataSet)
        {
            builder.AppendLine(Indent(indent) + $"SimpleSeries,");
            builder.AppendLine(Indent(indent) + $"ymode=log,");
            builder.AppendLine(Indent(indent) + $"xmode=log,");
            builder.AppendLine(Indent(indent) + $"xminorticks=true,");
            builder.AppendLine(Indent(indent) + $"yminorticks=true,");
            builder.AppendLine(Indent(indent) + $"log basis x={{2}},");
            builder.AppendLine(Indent(indent) + $"log basis y={{2}},");
            builder.AppendLine(Indent(indent) + $"log ticks with fixed point,");
            builder.AppendLine(Indent(indent) + $"x tick label style={{");
            builder.AppendLine(Indent(indent) + $"    /pgf/number format/1000 sep=");
            builder.AppendLine(Indent(indent) + $"}},");
            builder.AppendLine(Indent(indent) + $"log number format basis/.code 2 args={{$2^{{\\pgfmathprintnumber{{#2}}}}$}},");
            builder.AppendLine(Indent(indent) + $"every axis plot/.append style={{semithick}},");
            builder.AppendLine(Indent(indent) + $"scaled ticks=false,");
        }

        private static void AddXLogAxis(int indent, StringBuilder builder, DataSet dataSet)
        {
            builder.AppendLine(Indent(indent) + $"SimpleSeries,");
            builder.AppendLine(Indent(indent) + $"xmode=log,");
            builder.AppendLine(Indent(indent) + $"xminorticks=true,");
            builder.AppendLine(Indent(indent) + $"log basis x={{2}},");
            builder.AppendLine(Indent(indent) + $"log ticks with fixed point,");
            builder.AppendLine(Indent(indent) + $"x tick label style={{");
            builder.AppendLine(Indent(indent) + $"    /pgf/number format/1000 sep=");
            builder.AppendLine(Indent(indent) + $"}},");
            builder.AppendLine(Indent(indent) + $"log number format basis/.code 2 args={{$2^{{\\pgfmathprintnumber{{#2}}}}$}},");
            builder.AppendLine(Indent(indent) + $"every axis plot/.append style={{semithick}},");
            builder.AppendLine(Indent(indent) + $"scaled ticks=false,");
            builder.AppendLine(Indent(indent) + $"y tick label style={{");
            builder.AppendLine(Indent(indent) + $"    /pgf/number format/.cd,");
            builder.AppendLine(Indent(indent) + $"    fixed,");
            builder.AppendLine(Indent(indent) + $"    fixed zerofill,");
            builder.AppendLine(Indent(indent) + $"    precision={(dataSet.YIsInt ? 0 : 2)},");
            builder.AppendLine(Indent(indent) + $"    /tikz/.cd,");
            builder.AppendLine(Indent(indent) + $"}},");
        }

        private static void AddSimpleAxis(int indent, StringBuilder builder, SingleDataSet dataSet)
        {
            builder.AppendLine(Indent(indent) + $"SimpleSeries,");
            builder.AppendLine(Indent(indent) + $"every axis plot/.append style={{semithick}},");
            builder.AppendLine(Indent(indent) + $"scaled ticks=false,");
            builder.AppendLine(Indent(indent) + $"y tick label style={{");
            builder.AppendLine(Indent(indent) + $"    /pgf/number format/.cd,");
            builder.AppendLine(Indent(indent) + $"    fixed,");
            builder.AppendLine(Indent(indent) + $"    fixed zerofill,");
            builder.AppendLine(Indent(indent) + $"    precision={(dataSet.YIsInt ? 0 : 2)},");
            builder.AppendLine(Indent(indent) + $"    /tikz/.cd,");
            builder.AppendLine(Indent(indent) + $"}},");
            builder.AppendLine(Indent(indent) + $"x tick label style={{");
            builder.AppendLine(Indent(indent) + $"    /pgf/number format/.cd,");
            builder.AppendLine(Indent(indent) + $"    fixed,");
            builder.AppendLine(Indent(indent) + $"    fixed zerofill,");
            builder.AppendLine(Indent(indent) + $"    precision={(dataSet.XIsInt ? 0 : 2)},");
            builder.AppendLine(Indent(indent) + $"    /tikz/.cd,");
            builder.AppendLine(Indent(indent) + $"}},");
        }

        private static void Add3DAxis(int indent, StringBuilder builder, SingleDataSet dataSet)
        {
            builder.AppendLine(Indent(indent) + $"every axis plot/.append style={{semithick}},");
            builder.AppendLine(Indent(indent) + $"label style={{font=\\footnotesize}},");
            builder.AppendLine(Indent(indent) + $"tick label style={{font=\\footnotesize}},");
            builder.AppendLine(Indent(indent) + $"scaled ticks=false,");
            builder.AppendLine(Indent(indent) + $"grid=major,");
            builder.AppendLine(Indent(indent) + $"grid style={{dotted}},");
            builder.AppendLine(Indent(indent) + $"mesh/ordering=x varies,");
            builder.AppendLine(Indent(indent) + $"mesh/cols={dataSet.NumValues},");
            builder.AppendLine(Indent(indent) + $"y dir = reverse,");
            builder.AppendLine(Indent(indent) + $"ytick={{{string.Join( ",", Enumerable.Range(0, dataSet.NumLegendEntries).Where((x,i) => i % 2 == 0).ToArray())}}},");
            builder.AppendLine(Indent(indent) + $"yticklabels={{{string.Join( ",", dataSet.LegendNames.Where((x,i) => i % 2 == 0).ToArray())}}},");
        }
        
        private static void AddPlotElements(int indent, StringBuilder builder, SingleDataSet dataSet, bool legend, int subSetIndex = 0, int legendStart = 0)
        {
            if (legendStart > 0)
            {
                builder.AppendLine(Indent(indent) + $"\\pgfplotsset{{cycle list shift={legendStart}}}");
            }

            var normalisationFactors = dataSet.Normalise ? CalculateNormalisationFactors(dataSet) : Enumerable.Repeat(1.0f, dataSet.NumImages).ToArray();
            var means = CalculateImageMeans(dataSet, normalisationFactors);
            var deviations = CalculateImageDeviations(dataSet, normalisationFactors);

            for(var legendIndex = legendStart; legendIndex < dataSet.NumLegendEntries; legendIndex++)
            {
                builder.AppendLine(Indent(indent) + $"\\addplot + [");
                builder.AppendLine(Indent(indent) + "    error bars/.cd,");
                builder.AppendLine(Indent(indent) + "    y dir=both,y explicit,");
                builder.AppendLine(Indent(indent) + $"] coordinates {{");

                for(var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                {
                    AddValue(indent + 1, builder, dataSet.XValues[valueIndex], means[subSetIndex, legendIndex, valueIndex], deviations[subSetIndex, legendIndex, valueIndex]);
                }

                builder.AppendLine(Indent(indent) + $"}};");

                if (legend)
                {
                    AddLegendEntry(indent, builder, dataSet.LegendNames[legendIndex]);
                }
            } 
        }
        
        private static void Add3DPlotElements(int indent, StringBuilder builder, SingleDataSet dataSet, string contourDir = "", float contourLoc = 0)
        {
            var normalisationFactors = dataSet.Normalise ? CalculateNormalisationFactors(dataSet) : Enumerable.Repeat(1.0f, dataSet.NumImages).ToArray();
            var means = CalculateImageMeans(dataSet, normalisationFactors);
            var deviations = CalculateImageDeviations(dataSet, normalisationFactors);
            
            if (contourDir == "x")
            {
                builder.AppendLine(Indent(indent) + $"\\addplot3[");
                builder.AppendLine(Indent(indent) + $"    contour gnuplot={{");
                builder.AppendLine(Indent(indent) + $"        number=10,");
                builder.AppendLine(Indent(indent) + $"        labels=false,");
                builder.AppendLine(Indent(indent) + $"        contour dir=x,");
                builder.AppendLine(Indent(indent) + $"        draw color = black,");
                builder.AppendLine(Indent(indent) + $"    }},");
                builder.AppendLine(Indent(indent) + $"    x filter/.code=\\def\\pgfmathresult{{{contourLoc}}},");
                builder.AppendLine(Indent(indent) + $"    point meta rel=per plot,");
                builder.AppendLine(Indent(indent) + $"] coordinates {{");
            }
            else if (contourDir == "y")
            {
                builder.AppendLine(Indent(indent) + $"\\addplot3[");
                builder.AppendLine(Indent(indent) + $"    contour gnuplot={{");
                builder.AppendLine(Indent(indent) + $"        output point meta=rawy,");
                builder.AppendLine(Indent(indent) + $"        number=10,");
                builder.AppendLine(Indent(indent) + $"        labels=false,");
                builder.AppendLine(Indent(indent) + $"        contour dir=y,");
                builder.AppendLine(Indent(indent) + $"    }},");
                builder.AppendLine(Indent(indent) + $"    y filter/.code=\\def\\pgfmathresult{{{contourLoc}}},");
                builder.AppendLine(Indent(indent) + $"    colormap name = rainbow,");
                builder.AppendLine(Indent(indent) + $"    point meta rel=per plot,");
                builder.AppendLine(Indent(indent) + $"] coordinates {{");
            }
            else if (contourDir == "z")
            {
                builder.AppendLine(Indent(indent) + $"\\addplot3[");
                builder.AppendLine(Indent(indent) + $"    contour gnuplot={{");
                builder.AppendLine(Indent(indent) + $"        output point meta=rawz,");
                builder.AppendLine(Indent(indent) + $"        number=10,");
                builder.AppendLine(Indent(indent) + $"        labels=false,");
                builder.AppendLine(Indent(indent) + $"        contour dir=z,");
                builder.AppendLine(Indent(indent) + $"    }},");
                builder.AppendLine(Indent(indent) + $"    z filter/.code=\\def\\pgfmathresult{{{contourLoc}}},");
                builder.AppendLine(Indent(indent) + $"    colormap name = redgrey,");
                builder.AppendLine(Indent(indent) + $"    point meta rel=per plot,");
                builder.AppendLine(Indent(indent) + $"] coordinates {{");
            }
            else
            {
                builder.AppendLine(Indent(indent) + $"\\addplot3[");
                builder.AppendLine(Indent(indent) + $"    surf,");
                builder.AppendLine(Indent(indent) + $"    shader=faceted interp,");
                builder.AppendLine(Indent(indent) + $"    samples=2,");
                builder.AppendLine(Indent(indent) + $"    patch type=bilinear,");
                builder.AppendLine(Indent(indent) + $"    colormap name = redgrey,");
                builder.AppendLine(Indent(indent) + $"    point meta=z,");
                builder.AppendLine(Indent(indent) + $"    point meta rel=per plot,");
                builder.AppendLine(Indent(indent) + $"] coordinates {{");
            }

            for(var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
            {
                for(var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                {
                    Add3DValue(indent + 1, builder, dataSet.XValues[valueIndex], means[0, legendIndex, valueIndex], legendIndex);
                }

                builder.AppendLine(Indent(indent));
            } 

            builder.AppendLine(Indent(indent) + $"}};");
        }
        
        private static void AddFitElements(int indent, StringBuilder builder, FittedDataSet dataSet, bool legend, int subSetIndex = 0, int legendStart = 0)
        {
            var normalisationFactors = dataSet.Normalise ? CalculateNormalisationFactors(dataSet) : Enumerable.Repeat(1.0f, dataSet.NumImages).ToArray();
            var means = CalculateImageMeans(dataSet, normalisationFactors);
            var deviations = CalculateImageDeviations(dataSet, normalisationFactors);

            for (var legendIndex = legendStart; legendIndex < dataSet.NumLegendEntries; legendIndex++)
            {
                builder.AppendLine(Indent(indent) + $"\\addplot + [");
                builder.AppendLine(Indent(indent) + "    error bars/.cd,");
                builder.AppendLine(Indent(indent) + "    y dir=both,y explicit,");
                builder.AppendLine(Indent(indent) + $"] coordinates {{");

                var yValues = new double[dataSet.NumValues];
                var xValues = new double[dataSet.NumValues];
                for (var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                {
                    yValues[valueIndex] = means[subSetIndex, legendIndex, valueIndex];
                    xValues[valueIndex] = dataSet.XValues[valueIndex];
                }

                Func<double, double> fitted = Fit.LinearCombinationFunc(xValues, yValues, x => 1.0, dataSet.FitFunctions[legendIndex]);

                for (var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                {
                    AddValue(indent + 1, builder, dataSet.XValues[valueIndex], (float) fitted(dataSet.XValues[valueIndex]));
                }

                builder.AppendLine(Indent(indent) + $"}};");

                if (legend)
                {
                    AddLegendEntry(indent, builder, dataSet.FitLegendNames[legendIndex]);
                }
            }
        }

        private static void AddCrossSectionPlotElements(int indent, StringBuilder builder, CrossSectionDataSet dataSet, bool legend, bool skipLast, int valueIndex, int subSetIndex = 0)
        {
            for (var childIndex = 0; childIndex < dataSet.ChildDataSetTypes.Length; childIndex++)
            {
                var normalisationFactors = dataSet.Normalise ? CalculateNormalisationFactors(dataSet[childIndex]) : Enumerable.Repeat(1.0f, dataSet[0].NumImages).ToArray();
                var means = CalculateImageMeans(dataSet[childIndex], normalisationFactors);
                var deviations = CalculateImageDeviations(dataSet[childIndex], normalisationFactors);
                
                builder.AppendLine(Indent(indent) + $"\\addplot + [");
                builder.AppendLine(Indent(indent) + "    error bars/.cd,");
                builder.AppendLine(Indent(indent) + "    y dir=both,y explicit,");
                builder.AppendLine(Indent(indent) + $"] coordinates {{");

                for (var legendIndex = 0; legendIndex < (skipLast ? dataSet[0].NumLegendEntries - 1 : dataSet[0].NumLegendEntries); legendIndex++)
                {
                    AddValue(indent + 1, builder, dataSet.XValues[legendIndex], means[subSetIndex, legendIndex, valueIndex], deviations[subSetIndex, legendIndex, valueIndex]);
                }

                builder.AppendLine(Indent(indent) + $"}};");

                if (legend)
                {
                    AddLegendEntry(indent, builder, dataSet.LegendNames[childIndex]);
                }
            }
        }
        
        private static void AddConvergencePlotElements(int indent, StringBuilder builder, CrossSectionDataSet dataSet, bool legend, bool skipLast, int subSetIndex = 0)
        {
            for (var childIndex = 0; childIndex < dataSet.ChildDataSetTypes.Length; childIndex++)
            {
                var means = CalculateConvergenceMeans(dataSet[childIndex]);
                var deviations = CalculateConvergenceDeviations(dataSet[childIndex]);
                
                builder.AppendLine(Indent(indent) + $"\\addplot + [");
                builder.AppendLine(Indent(indent) + "    error bars/.cd,");
                builder.AppendLine(Indent(indent) + "    y dir=both,y explicit,");
                builder.AppendLine(Indent(indent) + $"] coordinates {{");

                for (var legendIndex = 0; legendIndex < (skipLast ? dataSet[0].NumLegendEntries - 1 : dataSet[0].NumLegendEntries); legendIndex++)
                {
                    AddValue(indent + 1, builder, dataSet.XValues[legendIndex], means[subSetIndex, legendIndex], deviations[subSetIndex, legendIndex]);
                }

                builder.AppendLine(Indent(indent) + $"}};");

                if (legend)
                {
                    AddLegendEntry(indent, builder, dataSet.LegendNames[childIndex]);
                }
            }
        }
        
        private static void AddValue(int indent, StringBuilder builder, float x, float y)
        {
            builder.AppendLine(Indent(indent) + $"({x,12}, {y,12})");
        }

        private static void AddValue(int indent, StringBuilder builder, float x, float y, float deviation)
        {
            const float tol = 10e-8f;

            if (float.IsNaN(deviation) || float.IsInfinity(deviation) || Math.Abs(deviation) < tol)
            {
                AddValue(indent, builder, x, y);
            }
            else
            {
                builder.AppendLine(Indent(indent) + $"({x,12}, {y,12}) +- ({0,12}, {2 * deviation,12})");
            }
        }
        
        private static void Add3DValue(int indent, StringBuilder builder, float x, float y, float z)
        {
            builder.AppendLine(Indent(indent) + $"({x,12}, {z,12}, {y,12})");
        }

        private static void AddLegendEntry(int indent, StringBuilder builder, string entry)
        {
            builder.AppendLine(Indent(indent) + $"\\addlegendentry{{{entry}}}");
        }

        private static void AddMessage(StringBuilder builder, string value, bool splitGraphFirst = false)
        {
            builder.AppendLine($"    \\node[anchor=north east] at ({(splitGraphFirst ? 5.8 : 13.2)},4.85) {{\\textit{{\\footnotesize{{{value}}}}}}};");
        }
        
        private static Dictionary<LegendLocation, string> _legendLocationStrings = new Dictionary<LegendLocation, string>()
        {
            {LegendLocation.NorthEast, "north east"},
            {LegendLocation.NorthWest, "north west"},
            {LegendLocation.SouthEast, "south east"},
            {LegendLocation.SouthWest, "south west"},
        };
        
        private static Dictionary<LegendLocation, string> _legendCoordinateStrings = new Dictionary<LegendLocation, string>()
        {
            {LegendLocation.NorthEast, "(1.05,0.95)"},
            {LegendLocation.NorthWest, "(0.03,0.95)"},
            {LegendLocation.SouthEast, "(1.05,0.05)"},
            {LegendLocation.SouthWest, "(0.03,0.05)"},
        };

        private static void AddLegend(int indent, StringBuilder builder, LegendLocation location)
        {
            if (location == LegendLocation.None) 
                return;
            builder.AppendLine(Indent(indent) + $"legend style={{at={{{_legendCoordinateStrings[location]}}},");
            builder.AppendLine(Indent(indent) + $"    anchor={_legendLocationStrings[location]},legend columns=1}},");
        }
        
        private static string Indent(int level)
        {
            return new string(' ', 4 * level);
        }

        private static float[,,,] CalculateSeriesMeans(SingleDataSet dataSet)
        {
            var result = new float[dataSet.NumSubSets, dataSet.NumLegendEntries, dataSet.NumImages, dataSet.NumValues];
            for(var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                for(var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
                {
                    for(var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                    {
                        for(var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                        {
                            for(var seriesIndex = 0; seriesIndex < dataSet.NumSeries; seriesIndex++)
                            {
                                result[subSetIndex, legendIndex, imageIndex, valueIndex] +=
                                    dataSet[subSetIndex, legendIndex, imageIndex, seriesIndex, valueIndex];
                            }

                            result[subSetIndex, legendIndex, imageIndex, valueIndex] /= dataSet.NumSeries;
                        }
                    }
                }
            }

            return result;
        }

        private static float[,,] CalculateImageMeans(SingleDataSet dataSet)
        {
            return CalculateImageMeans(dataSet, Enumerable.Repeat(1.0f, dataSet.NumImages).ToArray());
        }

        private static float[,,] CalculateImageMeans(SingleDataSet dataSet, float[] normalisationFactors)
        {
            var result = new float[dataSet.NumSubSets, dataSet.NumLegendEntries, dataSet.NumValues];
            for(var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                for(var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
                {
                    for(var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                    {
                        for(var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                        {
                            for(var seriesIndex = 0; seriesIndex < dataSet.NumSeries; seriesIndex++)
                            {
                                result[subSetIndex, legendIndex, valueIndex] +=
                                    dataSet[subSetIndex, legendIndex, imageIndex, seriesIndex, valueIndex] * normalisationFactors[imageIndex];
                            }
                        }

                        result[subSetIndex, legendIndex, valueIndex] /= dataSet.NumSeries;
                        result[subSetIndex, legendIndex, valueIndex] /= dataSet.NumImages;
                    }
                }
            }

            return result;
        }

        private static float[,,,] CalculateSeriesDeviations(SingleDataSet dataSet)
        {
            var means = CalculateSeriesMeans(dataSet);
            var result = new float[dataSet.NumSubSets, dataSet.NumLegendEntries, dataSet.NumImages, dataSet.NumValues];
            for(var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                for(var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
                {
                    for(var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                    {
                        for(var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                        {
                            for(var seriesIndex = 0; seriesIndex < dataSet.NumSeries; seriesIndex++)
                            {
                                result[subSetIndex, legendIndex, imageIndex, valueIndex] +=
                                    (float) Math.Pow(dataSet[subSetIndex, legendIndex, imageIndex, seriesIndex, valueIndex] - means[subSetIndex, legendIndex, imageIndex, valueIndex], 2);
                            }

                            result[subSetIndex, legendIndex, imageIndex, valueIndex] =
                                (float) Math.Sqrt(result[subSetIndex, legendIndex, imageIndex, valueIndex] / (dataSet.NumSeries - 1));
                        }
                    }
                }
            }

            return result;
        }

        private static float[,,] CalculateImageDeviations(SingleDataSet dataSet)
        {
            return CalculateImageDeviations(dataSet, Enumerable.Repeat(1.0f, dataSet.NumImages).ToArray());
        }

        private static float[,,] CalculateImageDeviations(SingleDataSet dataSet, float[] normalisationFactors)
        {
            var means = CalculateImageMeans(dataSet, normalisationFactors);
            var result = new float[dataSet.NumSubSets, dataSet.NumLegendEntries, dataSet.NumValues];
            for(var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                for(var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
                {
                    for(var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                    {
                        for(var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                        {
                            for(var seriesIndex = 0; seriesIndex < dataSet.NumSeries; seriesIndex++)
                            {
                                result[subSetIndex, legendIndex, valueIndex] +=
                                    (float) Math.Pow(dataSet[subSetIndex, legendIndex, imageIndex, seriesIndex, valueIndex] * normalisationFactors[imageIndex] - means[subSetIndex, legendIndex, valueIndex], 2);
                            }

                            result[subSetIndex, legendIndex, valueIndex] =
                                (float) Math.Sqrt(result[subSetIndex, legendIndex, valueIndex] / (dataSet.NumSeries * dataSet.NumImages - 1));
                        }
                    }
                }
            }

            return result;
        }
        
        private static float[] CalculateNormalisationFactors(SingleDataSet dataSet)
        {
            var means = CalculateSeriesMeans(dataSet);
            var result = new float[dataSet.NumImages];
            var sums = new double[dataSet.NumImages];
            for(var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                for(var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
                {
                    for(var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                    {
                        for(var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                        {
                            sums[imageIndex] +=
                                    means[subSetIndex, legendIndex, imageIndex, valueIndex];
                        }
                    }
                }
            }
            
            result[0] = 1.0f;
            for(var imageIndex = 1; imageIndex < dataSet.NumImages; imageIndex++)
            {
                result[imageIndex] = (float) (sums[0] / sums[imageIndex]);
            }

            return result;
        }

        private static int[,,] CalculateConvergencePoints(SingleDataSet dataSet)
        {
            const float maxDifference = 0.01f;
            const int iterationsForConvergence = 5;

            var means = CalculateSeriesMeans(dataSet);
            var result = new int[dataSet.NumSubSets, dataSet.NumLegendEntries, dataSet.NumImages];

            for(var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                for(var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
                {
                    for(var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                    {
                        var flagged = false;
                        for(var valueIndex = 0; valueIndex < dataSet.NumValues - iterationsForConvergence; valueIndex++)
                        {
                            var divergent = false;
                            for(var forwardPeekIndex = valueIndex + 1; forwardPeekIndex < valueIndex + iterationsForConvergence; forwardPeekIndex++)
                            {
                                if (!(Math.Abs(means[subSetIndex, legendIndex, imageIndex, forwardPeekIndex] - means[subSetIndex, legendIndex, imageIndex, valueIndex]) > maxDifference)) continue;
                                divergent = true;
                                break;
                            }

                            if (divergent) continue;
                            result[subSetIndex, legendIndex, imageIndex] = valueIndex;
                            flagged = true;
                            break;
                        }
                    
                        if (!flagged) result[subSetIndex, legendIndex, imageIndex] = -100000;
                    }
                }
            }
            
            return result;
        }

        private static float[,] CalculateConvergenceMeans(SingleDataSet dataSet)
        {
            var points = CalculateConvergencePoints(dataSet);
            
            var result = new float[dataSet.NumSubSets, dataSet.NumLegendEntries];

            for(var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                for(var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
                {
                    for(var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                    {
                        result[subSetIndex, legendIndex] +=
                            points[subSetIndex, legendIndex, imageIndex];
                    }

                    result[subSetIndex, legendIndex] /= dataSet.NumImages;
                }
            }
            
            return result;
        }

        private static float[,] CalculateConvergenceDeviations(SingleDataSet dataSet)
        {
            var points = CalculateConvergencePoints(dataSet);
            var means = CalculateConvergenceMeans(dataSet);
            var result = new float[dataSet.NumSubSets, dataSet.NumLegendEntries];

            for(var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                for(var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
                {
                    for(var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                    {
                        result[subSetIndex, legendIndex] += (float) Math.Pow(points[subSetIndex, legendIndex, imageIndex], 2);
                    }
                    
                    result[subSetIndex, legendIndex] =
                        (float) Math.Sqrt(result[subSetIndex, legendIndex] / (dataSet.NumImages - 1));
                }
            }
            
            return result;
        }
    }
}
