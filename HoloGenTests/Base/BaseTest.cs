﻿// Copyright 2018 (C) Peter J. Christopher - All Rights Reserved.
// 
// Unauthorized copying of this application, via any medium is strictly prohibited.
// Proprietary and confidential.
// 
// Please contact Peter J. Christopher in the first instance for inquiries of any kind. 
// Failing this, The Centre of Molecular Materials for Photonics and Electronics (CMMPE) 
// at the University of Cambridge may also be able to assist.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Numerics;
using HoloGen.Alg.Base.DataSets;
using HoloGen.Alg.Base.Enums;
using HoloGen.Alg.Base;
using HoloGen.Alg.Base.Managed;
using HoloGen.Controller;
using HoloGen.Image;
using HoloGen.Options;
using HoloGen.Utils;

namespace HoloGen.Alg.Base
{
    public static class BaseTest
    {
        public static readonly DirectoryInfo InputPath  = new DirectoryInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs");
        public static readonly DirectoryInfo ResultPath = new DirectoryInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Results");
        public static readonly DirectoryInfo GraphPath  = new DirectoryInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Graphs");
        
        public static readonly FileInfo[] TestImages =
        {
            new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs\\Lenna.hfimg"),
            new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs\\Mandrill.hfimg"),
            new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs\\Peppers.hfimg"),
            new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs\\Man.hfimg"),
            new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs\\CameraMan.hfimg"),
            new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs\\Landscape.hfimg"),
            new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs\\Aerial.hfimg"),
            //new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs\\Target.hfimg")
        };

        public static readonly FileInfo Lenna = new FileInfo("C:\\Users\\palav\\Documents\\Repos\\PhD\\Benchmark\\Inputs\\Lenna.hfimg");
        
        private static readonly LoggerDelegate SilentLoggerDelegate = SilentLog;
        
        public static DataSet RunTest(DataSet dataSet, Action<string> log)
        {
            switch (dataSet)
            {
                case ErrorVsItrPerLevel convergenceDataSet:
                    return RunSpecificTest(convergenceDataSet, log);
                case FourierDataSet fourierDataSet:
                    return RunSpecificTest(fourierDataSet, log);
                case ErrorVsLevelCrossSection _:
                    return dataSet; 
                case ItrVsLevelCrossSection _:
                    return dataSet; 
                case TimeVsResPerLevel resolutionTimeComparisonDataSet:
                    return RunSpecificTest(resolutionTimeComparisonDataSet, log);
                case ErrorVsResPerItr resolutionErrorComparisonDataSet:
                    return RunSpecificTest(resolutionErrorComparisonDataSet, log);
                case ConvergenceSpeedFractionVsLevel convergenceSpeedFractionVsLevel:
                    return RunSpecificTest(convergenceSpeedFractionVsLevel, log);
                default:
                    throw new NotImplementedException("Unknown Data Set Type");
            }
        }
        
        private static ErrorVsItrPerLevel RunSpecificTest(ErrorVsItrPerLevel dataSet, Action<string> log)
        {
            var options = ParseOptions(dataSet.Algorithm, dataSet.ModulationScheme, dataSet.Seed);
            
            options = SetResultEvery(options, dataSet.Spacing);
            
            Action<Dictionary<MetricType, float>> newMetrics = metrics => { };

            var totalTests = dataSet.NumLegendEntries * dataSet.NumImages * dataSet.NumSeries;
            var currentTest = 0;
            for(var legendIndex = 0; legendIndex < dataSet.Levels.Length; legendIndex++)
            {
                options = SetLevel(options, dataSet.Levels[legendIndex]);
                for (var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                {
                    options = SetImage(options, dataSet.Images[imageIndex]);
                    for(var seriesIndex = 0; seriesIndex < dataSet.NumSeries; seriesIndex++)
                    {
                        log($"Running sub test {1 + currentTest++} of {totalTests}...");
                        using (var controller = new AlgorithmController(options, SilentLog, newMetrics))
                        {
                            controller.Alg.RunStartup();
                            for (var valueIndex = 0; valueIndex < dataSet.NumValues; valueIndex++)
                            {
                                var metrics = controller.Alg.RunIterations();
                                dataSet[legendIndex, imageIndex, seriesIndex, valueIndex] = metrics[MetricType.MeanSquaredError];
                            }
                            controller.Alg.RunCleanup();
                        }
                    }
                }
            }

            return dataSet;
        }

        private static FourierDataSet RunSpecificTest(FourierDataSet dataSet, Action<string> log)
        {
            var totalTests = dataSet.Resolutions.Length * dataSet.NumSeries;
            var currentTest = 0;

            // Run a test once to ensure everything is loaded
            TestHandlerMan.TestFFT(2, dataSet.NumIterations, SilentLoggerDelegate, 1);
            
            var stopWatch = new Stopwatch();
            for (var resolutionIndex = 0; resolutionIndex < dataSet.Resolutions.Length; resolutionIndex++)
            {
                for (var seriesIndex = 0; seriesIndex < dataSet.NumSeries; seriesIndex++)
                {
                    log($"Running sub test {1 + currentTest++} of {totalTests}...");

                    stopWatch.Start();
                    var microseconds = TestHandlerMan.TestFFT(dataSet.Resolutions[resolutionIndex], dataSet.NumIterations, SilentLoggerDelegate, 1);
                    stopWatch.Stop();
                    // ReSharper disable once PossibleLossOfFraction
                    dataSet[0, 0, seriesIndex, resolutionIndex] = microseconds / 1000f / dataSet.NumIterations;
                    stopWatch.Reset();
                }
            }

            //todo: add fit here???

            return dataSet;
        }
        
        private static TimeVsResPerLevel RunSpecificTest(TimeVsResPerLevel dataSet, Action<string> log)
        {
            var options = ParseOptions(dataSet.Algorithm, dataSet.ModulationScheme, dataSet.Seed);
            
            options = SetResultEvery(options, dataSet.NumIterations);
            Action<Dictionary<MetricType, float>> newMetrics = metrics => { };

            var totalTests = dataSet.Levels.Length * dataSet.NumSeries * dataSet.NumImages;
            var currentTest = 0;
            var stopWatch = new Stopwatch();
            for(var legendIndex = 0; legendIndex < dataSet.Levels.Length; legendIndex++)
            {
                options = SetLevel(options, dataSet.Levels[legendIndex]);
                for(var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                {
                    options = SetImage(options, dataSet.Images[imageIndex]);
                    for(var seriesIndex = 0; seriesIndex < dataSet.NumSeries; seriesIndex++)
                    {
                        log($"Running sub test {1 + currentTest++} of {totalTests}...");
                        for(var resolutionIndex = 0; resolutionIndex < dataSet.Resolutions.Length; resolutionIndex++)
                        {
                            options = SetImage(options, dataSet.Images[imageIndex], dataSet.Resolutions[resolutionIndex]);
                            options = SetResolution(options, dataSet.Resolutions[resolutionIndex]);
                            using(var controller = new AlgorithmController(options, SilentLog, newMetrics))
                            {
                                controller.Alg.RunStartup();
                                stopWatch.Start();
                                controller.Alg.RunIterations();
                                stopWatch.Stop();
                                dataSet[legendIndex, imageIndex, seriesIndex, resolutionIndex] =
                                    (float)stopWatch.Elapsed.TotalMilliseconds / dataSet.NumIterations;
                                stopWatch.Reset();
                                controller.Alg.RunCleanup();
                            }
                        }
                    }
                }
            }

            return dataSet;
        }

        private static ErrorVsResPerItr RunSpecificTest(ErrorVsResPerItr dataSet, Action<string> log)
        {
            var options = ParseOptions(dataSet.Algorithm, dataSet.ModulationScheme, dataSet.Seed);
            
            Action<Dictionary<MetricType, float>> newMetrics = metrics => { };
            
            var totalTests = dataSet.NumSubSets * dataSet.NumLegendEntries * dataSet.NumSeries * dataSet.NumImages;
            var currentTest = 0;
            for (var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                options = SetLevel(options, dataSet.Levels[subSetIndex]);
                for (var legendIndex = 0; legendIndex < dataSet.NumLegendEntries; legendIndex++)
                {
                    options = SetResultEvery(options, dataSet.IterationCounts[legendIndex]);
                    for (var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                    {
                        options = SetImage(options, dataSet.Images[imageIndex]);
                        for (var seriesIndex = 0; seriesIndex < dataSet.NumSeries; seriesIndex++)
                        {
                            log($"Running sub test {1 + currentTest++} of {totalTests}...");
                            for (var resolutionIndex = 0; resolutionIndex < dataSet.Resolutions.Length; resolutionIndex++)
                            {
                                options = SetImage(options, dataSet.Images[imageIndex], dataSet.Resolutions[resolutionIndex]);
                                options = SetResolution(options, dataSet.Resolutions[resolutionIndex]);
                                using(var controller = new AlgorithmController(options, SilentLog, newMetrics))
                                {
                                    controller.Alg.RunStartup();
                                    var metrics = controller.Alg.RunIterations();
                                    dataSet[subSetIndex, legendIndex, imageIndex, seriesIndex, resolutionIndex] = metrics[MetricType.MeanSquaredError];
                                    controller.Alg.RunCleanup();
                                }
                            }
                        }
                    }
                }
            }

            return dataSet;
        }
        
        private static ConvergenceSpeedFractionVsLevel RunSpecificTest(ConvergenceSpeedFractionVsLevel dataSet, Action<string> log)
        {
            var options = ParseOptions(dataSet.Algorithm, dataSet.ModulationScheme, dataSet.Seed);
            options = SetResultEvery(options, dataSet.NumIterations);
            
            Action<Dictionary<MetricType, float>> newMetrics = metrics => { };
            
            var totalTests = dataSet.NumSubSets * dataSet.Resolutions.Length * dataSet.NumSeries * dataSet.NumImages * dataSet.NumPixelsToSwitch.Length;
            var currentTest = 0;
            for (var subSetIndex = 0; subSetIndex < dataSet.NumSubSets; subSetIndex++)
            {
                options = SetLevel(options, dataSet.Levels[subSetIndex]);
                for (var resolutionIndex = 0; resolutionIndex < dataSet.Resolutions.Length; resolutionIndex++)
                {
                    options = SetResolution(options, dataSet.Resolutions[resolutionIndex]);
                    for (var imageIndex = 0; imageIndex < dataSet.NumImages; imageIndex++)
                    {
                        options = SetImage(options, dataSet.Images[imageIndex], dataSet.Resolutions[resolutionIndex]);
                        for (var seriesIndex = 0; seriesIndex < dataSet.NumSeries; seriesIndex++)
                        {
                            for (var valueIndex = 0; valueIndex < dataSet.NumPixelsToSwitch.Length; valueIndex++)
                            {
                                log($"Running sub test {1 + currentTest++} of {totalTests}...");
                                options = SetNumPixelsToSwitch(options, dataSet.NumPixelsToSwitch[valueIndex]);
                                using(var controller = new AlgorithmController(options, SilentLog, newMetrics))
                                {
                                    controller.Alg.RunStartup();
                                    var metrics = controller.Alg.RunIterations();
                                    var val1 = metrics[MetricType.MeanSquaredError];
                                    metrics = controller.Alg.RunIterations();
                                    var val2 = metrics[MetricType.MeanSquaredError];
                                    dataSet[subSetIndex, resolutionIndex, imageIndex, seriesIndex, valueIndex] = val2 - val1;
                                    controller.Alg.RunCleanup();
                                }
                            }
                        }
                    }
                }
            }

            return dataSet;
        }
        
        private static OptionsRoot SetImage(OptionsRoot options, FileInfo image)
        {
            options.TargetPage.TargetFolder.TargetFile.Value = image;

            return options;
        }
        
        private static OptionsRoot SetImage(OptionsRoot options, FileInfo image, int resolution)
        {
            options.TargetPage.TargetFolder.TargetFile.Value = 
                new FileInfo(image.Directory + "\\Scaled\\" + Path.GetFileNameWithoutExtension(image.Name) + "_" + resolution + ".hfimg");

            return options;
        }
        
        private static OptionsRoot SetResultEvery(OptionsRoot options, int value)
        {
            options.OutputPage.RunFolder.ShowResults.ResultEvery.Value = value;
            
            return options;
        }
        
        private static OptionsRoot SetNumPixelsToSwitch(OptionsRoot options, int value)
        {
            options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm.VariantOption2.Possibilities2.StandardDSVariant.NumPixelsToSwitch.Value = value;
            
            return options;
        }

        private static OptionsRoot SetLevel(OptionsRoot options, int level)
        {
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.SLMLevels.Value = level;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.SLMLevels.Value = level;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.MaximumAngle.Value = 2 * Math.PI * (level - 1) / level;

            return options;
        }

        private static OptionsRoot SetResolution(OptionsRoot options, int resolution)
        {
            options.ProjectorPage.HologramFolder.SLMResolutionX.Value = resolution;
            options.ProjectorPage.HologramFolder.SLMResolutionY.Value = resolution * 2;

            return options;
        }

        private static OptionsRoot ParseOptions(AlgorithmType? algorithmType, ModulationSchemeType? modulationSchemeType, SeedType? seedType)
        {
            OptionsRoot options = new OptionsRoot();
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousAmpSLM.MinimumLevel.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousAmpSLM.MaximumLevel.Value = 1;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousPhaseSLM.MinimumAngle.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousPhaseSLM.MaximumAngle.Value = Math.PI * 2;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.MinimumAngle.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.MaximumAngle.Value = Math.PI * 2;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.SLMLevels.Value = 10;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.MinimumLevel.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.MaximumLevel.Value = 1;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.SLMLevels.Value = 10;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryAmpSLM.MinimumLevel.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryAmpSLM.MaximumLevel.Value = 1;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryPhaseSLM.MinimumAngle.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryPhaseSLM.MaximumAngle.Value = Math.PI;
            options.TargetPage.TargetFolder.ScaleTargetImage.Value = true;
            options.ProjectorPage.IlluminationFolder.IlluminationPower.Value = 1.0;
            options.ProjectorPage.HologramFolder.SeedOption.Value = options.ProjectorPage.HologramFolder.SeedOption.Possibilities2.BackProjectSeed;
            options.ProjectorPage.HologramFolder.SLMResolutionX.Value = 512;
            options.ProjectorPage.HologramFolder.SLMResolutionY.Value = 1024;
            options.TargetPage.RegionFolder.RegionOption.Possibilities2.RegionAutoExpand.ExpandOption.Value =
                options.TargetPage.RegionFolder.RegionOption.Possibilities2.RegionAutoExpand.ExpandOption.Possibilities2.ExpandTopLeft;
            
            switch (algorithmType)
            {
                case AlgorithmType.GS:
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.GSAlgorithm;
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.GSAlgorithm.VariantOption2.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.GSAlgorithm.VariantOption2.Possibilities2.StandardGSVariant;
                    break;
                case AlgorithmType.DS:
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm;
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm.VariantOption2.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm.VariantOption2.Possibilities2.StandardDSVariant;
                    break;
                case AlgorithmType.DSF:
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm;
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm.VariantOption2.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm.VariantOption2.Possibilities2.FastDSVariant;
                    break;
                case AlgorithmType.SA:
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.SAAlgorithm;
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.SAAlgorithm.VariantOption2.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.SAAlgorithm.VariantOption2.Possibilities2.StandardSAVariant;
                    break;
                case AlgorithmType.SAF:
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.SAAlgorithm;
                    options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.SAAlgorithm.VariantOption2.Value = 
                        options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.SAAlgorithm.VariantOption2.Possibilities2.FastSAVariant;
                    break;
                case AlgorithmType.OSPR:
                    throw new Exception("Unknown algorithm");
                default:
                    throw new Exception("Unknown algorithm");
            }

            switch (modulationSchemeType)
            {
                case ModulationSchemeType.Phase:
                    options.ProjectorPage.HologramFolder.SLMTypeOption.Value = options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM;
                    break;
                case ModulationSchemeType.Amplitude:
                    options.ProjectorPage.HologramFolder.SLMTypeOption.Value = options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM;
                    break;
                default:
                    throw new Exception("Unknown modulation scheme");
            }

            switch (seedType)
            {
                case SeedType.Random:
                    options.ProjectorPage.HologramFolder.SeedOption.Value = options.ProjectorPage.HologramFolder.SeedOption.Possibilities2.RandomSeed;
                    break;
                case SeedType.BackProject:
                    options.ProjectorPage.HologramFolder.SeedOption.Value = options.ProjectorPage.HologramFolder.SeedOption.Possibilities2.BackProjectSeed;
                    break;
                default:
                    throw new Exception("Unknown seed");
            }

            return options;
        }

        public static int[] Resolutions(int sttPow, int maxPow, int every = 10)
        {
            Debug.Assert(every <= 10);
            Debug.Assert(every >= 1);

            // We try to give easy factorisations here to reduce the spread
            int[] resolutions = new int[(maxPow - sttPow)*every + 1];
            int idx = 0;
            for (int i = 0; i < maxPow - sttPow; i++)
            {
                int pow = i + sttPow;
                if(every >= 01) resolutions[idx++] = (int) Math.Pow(2, pow);             // 1
                if(every >= 09) resolutions[idx++] = (int) Math.Pow(2, pow - 5) * 7 * 5; // 1.09375
                if(every >= 05) resolutions[idx++] = (int) Math.Pow(2, pow - 3) * 3 * 3; // 1.125
                if(every >= 03) resolutions[idx++] = (int) Math.Pow(2, pow - 2) * 5;     // 1.25
                if(every >= 06) resolutions[idx++] = (int) Math.Pow(2, pow - 4) * 7 * 3; // 1.3125
                if(every >= 02) resolutions[idx++] = (int) Math.Pow(2, pow - 1) * 3;     // 1.5
                if(every >= 10) resolutions[idx++] = (int) Math.Pow(2, pow - 5) * 7 * 7; // 1.53125
                if(every >= 07) resolutions[idx++] = (int) Math.Pow(2, pow - 4) * 5 * 5; // 1.5625
                if(every >= 04) resolutions[idx++] = (int) Math.Pow(2, pow - 2) * 7;     // 1.75
                if(every >= 08) resolutions[idx++] = (int) Math.Pow(2, pow - 3) * 5 * 3; // 1.875
            }
            resolutions[resolutions.Length - 1] = (int) Math.Pow(2, maxPow);
            return resolutions;
        }
        
        public static void SilentLog(string message)
        {
        }
    }
}
