﻿// Copyright 2018 (C) Peter J. Christopher - All Rights Reserved.
// 
// Unauthorized copying of this application, via any medium is strictly prohibited.
// Proprietary and confidential.
// 
// Please contact Peter J. Christopher in the first instance for inquiries of any kind. 
// Failing this, The Centre of Molecular Materials for Photonics and Electronics (CMMPE) 
// at the University of Cambridge may also be able to assist.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using HoloGen.Controller;
using HoloGen.Image;
using HoloGen.IO;
using HoloGen.Options;
using HoloGen.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Numerics;
using HoloGen.Alg.Base;
using HoloGen.Alg.Base.Managed;

namespace HoloGen.Alg
{
    class AlgMain
    {
        // ReSharper disable once UnusedParameter.Local
        static void Main(string[] args)
        {
            if (!true)
            {
                InternalTests();
            }
            else
            {
                var tests = new BenchmarkingTests();
                tests.Run();

                // Keep the console window open in debug mode.
                Log("Press any key to exit.");
                //Console.ReadKey();
            }
        }

        static void InternalTests()
        {
            Console.WriteLine("Starting!");

            foreach(var file2 in new DirectoryInfo("C:\\Users\\palav\\Documents\\TESTOUTPUT").EnumerateFiles())
                file2.Delete();

            // Create options for run
            OptionsRoot options = new OptionsRoot();
            options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Value =
                options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.GSAlgorithm;
            options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm.VariantOption2.Value =
                options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm.VariantOption2.Possibilities2.FastDSVariant;
            options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.SAAlgorithm.VariantOption2.Value =
                options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.SAAlgorithm.VariantOption2.Possibilities2.FastSAVariant;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Value =
                options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM;
            options.ProjectorPage.HologramFolder.SLMResolutionX.Value = 512;
            options.ProjectorPage.HologramFolder.SLMResolutionY.Value = 1024;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousAmpSLM.MinimumLevel.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousAmpSLM.MaximumLevel.Value = 1;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousPhaseSLM.MinimumAngle.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.ContinuousPhaseSLM.MaximumAngle.Value = Math.PI * 2;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.MinimumAngle.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.MaximumAngle.Value = Math.PI * 4/3;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiPhaseSLM.SLMLevels.Value = 3;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.MinimumLevel.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.MaximumLevel.Value = 1;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.MultiAmpSLM.SLMLevels.Value = 10;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryAmpSLM.MinimumLevel.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryAmpSLM.MaximumLevel.Value = 1;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryPhaseSLM.MinimumAngle.Value = 0;
            options.ProjectorPage.HologramFolder.SLMTypeOption.Possibilities2.BinaryPhaseSLM.MaximumAngle.Value = Math.PI;
            options.ProjectorPage.HologramFolder.SeedOption.Value = options.ProjectorPage.HologramFolder.SeedOption.Possibilities2.RandomSeed;
            options.TargetPage.TargetFolder.TargetFile.Value = new FileInfo("C:\\Users\\palav\\Documents\\Lenna.hfimg");
            options.TargetPage.RegionFolder.RegionOption.Possibilities2.RegionAutoExpand.ExpandOption.Value =
                options.TargetPage.RegionFolder.RegionOption.Possibilities2.RegionAutoExpand.ExpandOption.Possibilities2.ExpandTopLeft;
            options.OutputPage.RunFolder.ShowResults.ResultEvery.Value = 1;
            options.TargetPage.TargetFolder.ScaleTargetImage.Value = true;
            options.ProjectorPage.IlluminationFolder.IlluminationPower.Value = 1.0;
            options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.DSAlgorithm.VariantOption2.Possibilities2.StandardDSVariant.NumPixelsToSwitch.Value = 5;
            options.AlgorithmsPage.AlgorithmsFolder.AlgorithmOption.Possibilities2.SAAlgorithm.VariantOption2.Possibilities2.StandardSAVariant.NumPixelsToSwitch.Value = 5;

            // Run the algorithm
            Bitmap bitmap;
            FileInfo file;
            
            //options.NumPixelsToSwitch.Value,
            //(float) options.StartingTemperature.Value,
            //(float) options.TemperatureCoefficient.Value
            //ImageCache cache2 = new ImageCache(LoadImage(new FileInfo("C:\\Users\\palav\\Documents\\Lenna.png")), false, new Scale(0, 255));
            //bitmap = cache2.GetAppropriateImage(TransformType.FFTShift, ImageViewType.Magnitude, ColorSchemes.Instance.WhiteonBlack, ImageScaleType.None).Image;
            //file = FileUtils.GetUniqueName(new DirectoryInfo("C:\\Users\\palav\\Documents\\TESTOUTPUT"), "test", "png");
            //bitmap.Save(file.FullName, ImageFormat.Png);

            Log("Running startup...");
            using (AlgorithmController controller = new AlgorithmController(options, Log, NewMetrics))
            {
                var success = controller.Alg.RunStartup();
                if (success != 0)
                {
                    Log("Running startup... FAILED!");
                }

                for (int i = 0; i < 30; i++)
                {
                    Log("Running iterations...");
                    var metrics = controller.Alg.RunIterations();
                    NewMetrics(metrics);

                    Log("Getting result...");
                    Complex[,] values = controller.Alg.GetResult();
                    //Export and save the exported image
                    ImageCache cache = new ImageCache(new ComplexImage(values, options));
                    bitmap = cache.GetAppropriateImage(TransformType.None, ImageViewType.Phase, ColorSchemes.Instance.WhiteonBlack, ImageScaleType.None).Image;
                    file = FileUtils.GetUniqueName(new DirectoryInfo("C:\\Users\\palav\\Documents\\TESTOUTPUT"), "phase", "png");
                    bitmap.Save(file.FullName, ImageFormat.Png);
                    bitmap = cache.GetAppropriateImage(TransformType.None, ImageViewType.Magnitude, ColorSchemes.Instance.WhiteonBlack, ImageScaleType.None).Image;
                    file = FileUtils.GetUniqueName(new DirectoryInfo("C:\\Users\\palav\\Documents\\TESTOUTPUT"), "mag", "png");
                    bitmap.Save(file.FullName, ImageFormat.Png);
                    file = FileUtils.GetUniqueName(new DirectoryInfo("C:\\Users\\palav\\Documents\\TESTOUTPUT"), "recon", "png");
                    bitmap = cache.GetAppropriateImage(TransformType.FFT, ImageViewType.Magnitude, ColorSchemes.Instance.WhiteonBlack, ImageScaleType.None).Image;
                    bitmap.Save(file.FullName, ImageFormat.Png);
                }


                Log("Running cleanup...");
                success = controller.Alg.RunCleanup();
                if (success != 0)
                {
                    Log("Running cleanup... FAILED!");
                }
            }
        }
        
        private static ComplexImage LoadImage(FileInfo file)
        {
            if (file.Extension.Equals(".hfimg"))
            {
                var res = new JsonImageFileLoader().Load(file);
                ComplexImage loaded = res.Item2;
                return loaded;
            }
            else
            {
                var image = System.Drawing.Image.FromFile(file.FullName);
                Bitmap bitmap = new Bitmap(image);

                var size = bitmap.Size;

                var bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, size.Width, size.Height),
                    ImageLockMode.ReadWrite,
                    PixelFormat.Format24bppRgb
                );
                var bitmapBytes = new byte[Math.Abs(bitmapData.Stride) * bitmapData.Height];
                System.Runtime.InteropServices.Marshal.Copy(bitmapData.Scan0, bitmapBytes, 0, Math.Abs(bitmapData.Stride) * bitmapData.Height);

                Complex[,] values = new Complex[size.Height, size.Width];

                long location = 0;
                for (int y = 0; y < size.Height; y++)
                {
                    for (int x = 0; x < size.Width; x++)
                    {
                        // Values from https://en.wikipedia.org/wiki/Grayscale
                        // WARNING: GDI+ Uses the order blue, green, red!
                        var norm = ((bitmapBytes[location + 2] * 30) + (bitmapBytes[location + 1] * 59) + (bitmapBytes[location] * 11)) / 100.0;
                        values[y, x] = new Complex(norm, 0);

                        location += 3;
                    }
                }

                bitmap.UnlockBits(bitmapData);
                return new ComplexImage(values);
            }
        }

        static void Log(string message)
        {
            string timedMessage = Time.Timestamp() + message;
            Console.WriteLine(timedMessage);
        }

        static void NewMetrics(Dictionary<MetricType, float> dict)
        {
            string collated = "";

            foreach(var entry in dict)
            {
                collated += Enum.GetName(typeof(MetricType), entry.Key) + " = " + entry.Value + " ";
            }
            Console.WriteLine("METRICS ::>> " + collated);
        }
    }
}
