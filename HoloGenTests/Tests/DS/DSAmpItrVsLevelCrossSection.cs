﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HoloGen.Alg.Base;
using HoloGen.Alg.Base.DataSets;

namespace HoloGen.Alg.Tests.DS
{
    public sealed class DSAmpItrVsLevelCrossSection : ItrVsLevelCrossSection
    {
        public override Type[] ChildDataSetTypes => new[]
        {
            typeof(DSAmpRandomErrorVsItrPerLevel),
            typeof(DSAmpBackProjectErrorVsItrPerLevel)
        };
    }
}
