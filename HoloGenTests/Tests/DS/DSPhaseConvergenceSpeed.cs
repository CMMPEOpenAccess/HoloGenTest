﻿using HoloGen.Alg.Base.DataSets;
using HoloGen.Alg.Base.Enums;

namespace HoloGen.Alg.Tests.DS
{
    public class DSPhaseConvergenceSpeed : ConvergenceSpeedFractionVsLevel
    {
        public override int NumIterations => 10000;
        public override ModulationSchemeType ModulationScheme => ModulationSchemeType.Phase;
        public override SeedType Seed => SeedType.BackProject;
        public override bool IsSplit => true;
    }
}
