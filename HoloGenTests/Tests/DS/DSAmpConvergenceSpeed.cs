﻿using HoloGen.Alg.Base.DataSets;
using HoloGen.Alg.Base.Enums;

namespace HoloGen.Alg.Tests.DS
{
    public class DSAmpConvergenceSpeed : ConvergenceSpeedFractionVsLevel
    {
        public override int NumIterations => 10000;
        public override ModulationSchemeType ModulationScheme => ModulationSchemeType.Amplitude;
        public override SeedType Seed => SeedType.BackProject;
        public override bool IsSplit => true;
    }
}
