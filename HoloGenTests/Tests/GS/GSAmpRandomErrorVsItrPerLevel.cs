﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HoloGen.Alg.Base;
using HoloGen.Alg.Base.DataSets.Specific;
using HoloGen.Alg.Base.Enums;

namespace HoloGen.Alg.Tests.GS
{
    public sealed class GSAmpRandomErrorVsItrPerLevel : GSErrorVsItrPerLevel
    {
        public override ModulationSchemeType ModulationScheme => ModulationSchemeType.Phase;
        public override SeedType Seed => SeedType.Random;
    }
}
