﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HoloGen.Alg.Base;
using HoloGen.Alg.Base.DataSets;
using Newtonsoft.Json;

namespace HoloGen.Alg.Tests
{
    public sealed class CUFFTResolutionBehaviour : FourierDataSet
    {
        public override int NumIterations => 500;
        public override string[] LegendNames => new[] {"fftshift $\\circ$ fft2 $\\circ$ fftshift"};
        public override string[] FitLegendNames => new[] {"Fit to $C + O(N^2log(N)^2)$"};
        [JsonIgnore] public override Func<double, double>[] FitFunctions => new Func<double, double>[]{x => Math.Pow(x * Math.Log(x), 2.0)};
    }
}
