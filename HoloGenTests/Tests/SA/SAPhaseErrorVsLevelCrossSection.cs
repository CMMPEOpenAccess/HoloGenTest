﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HoloGen.Alg.Base;
using HoloGen.Alg.Base.DataSets;

namespace HoloGen.Alg.Tests.SA
{
    public sealed class SAPhaseErrorVsLevelCrossSection : ErrorVsLevelCrossSection
    {
        public override Type[] ChildDataSetTypes => new[]
        {
            typeof(SAPhaseRandomErrorVsItrPerLevel),
            typeof(SAPhaseBackProjectErrorVsItrPerLevel)
        };
    }
}
