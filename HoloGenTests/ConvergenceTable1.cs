﻿// Copyright 2018 (C) Peter J. Christopher - All Rights Reserved.
// 
// Unauthorized copying of this application, via any medium is strictly prohibited.
// Proprietary and confidential.
// 
// Please contact Peter J. Christopher in the first instance for inquiries of any kind. 
// Failing this, The Centre of Molecular Materials for Photonics and Electronics (CMMPE) 
// at the University of Cambridge may also be able to assist.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk
using System;
using System.Collections.Generic;
using HoloGen.Alg;

namespace HoloGen.Alg
{
    //using L1Result = Tuple<string, List<Tuple<float, float>>>;
    //using L2Result = Tuple<string, List<Tuple<string, List<float>>>>;
    //using L3Result = Tuple<string, List<Tuple<string, List<Tuple<string, List<float>>>>>>;

    //public class ConvergenceTable1 : BaseTest
    //{
    //    public override Graph RunTest(Action<string> log)
    //    {
    //        Random rnd = new Random();

    //        Tuple<float, float>[][] result = new Tuple<float, float>[18][];
    //        for (int index1 = 0; index1 < result.Length; index1++)
    //        {
    //            result[index1] = new Tuple<float, float>[7];
    //            for (int index2 = 0; index2 < result[index1].Length; index2++)
    //            {
    //                result[index1][index2] = new Tuple<float, float>((float) rnd.NextDouble(), (float) rnd.NextDouble());
    //            }
    //        }
            
    //        return new Graph(GetType().Name)
    //            .AddStandardTable(result, new[]{"IFTA", "SA", "DBS"});
    //    }
    //}
}
