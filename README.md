# README

_**Warning! HoloGen is a work in progress developed as a tool to aid my (Peter Christopher) PhD. It is comparitively stable in the areas that I needed it for but there are areas where it is not yet feature complete. Use this at your own risk!**_

## HoloGen Test Suite

This repository contains the test and benchmarking suite for HoloGen. If you were looking for the application itself then you need to go [here](https://gitlab.com/CMMPEOpenAccess/HoloGen). 

There are no instructions for the test and benchmarking suite as while it is feature complete for the benchmarking it is fundamentally incomplete in terms of tests and is extremely ugly codewise. Please don't judge my programming skill based on this repo! 

Feel free to use anything you want but, be warned, here be dragons!

## License

Copyright 2019 Peter J. Christopher

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk